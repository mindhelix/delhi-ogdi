﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DpOgdiUI
{
    public class AppConfig
    {
        public static string DATASERVICE_URL = "http://ogdimht.cloudapp.net/v1/delhi/";
        public static string BLOB_URL = "http://mhtogdidata.blob.core.windows.net/sho-photos/";

        public static double MAP_DEFAULT_ZOOMLEVEL = 10.65;
        public static string MAP_KML_URL = DATASERVICE_URL+"PoliceStations?format=kml";

        public static Color MAP_TILE_FILL = Colors.Green;   // Map tile fill color in normal state
        public static Color MAP_TILE_STROKE = Colors.Black; // Map tile border stroke color
        public static double MAP_TILE_OPACITY = 0.5;
        public static double MAP_TILE_STROKE_THICKNESS = 1;

        public static Color MAP_TILE_HOVER_FILL = Colors.Green; // Map Tile Fill Color on mouseover
        public static Color MAP_TILE_HOVER_STROKE = Colors.Black; // Map tile border stroke color
        public static double MAP_TILE_HOVER_OPACITY = 0.7;
        public static double MAP_TILE_HOVER_STROKE_THICKNESS = 1;

        public static Color MAP_ROUTELINE_COLOR = Colors.Yellow;
        public static double MAP_ROUTELINE_OPACITY = 0.65;
        public static double MAP_ROUTELINE_THICKNESS = 5.0;

        public static Color FEEDBACK_PIN_COLOR = Colors.Yellow;
        
        public static string SEARCH_RESULT_PIN_STRING = "Searched Location";
        public static string STATION_PIN_STRING = "Police Station {0}";

        

        public static decimal[] BINGMAPS_GEO_FILTER_BOTTOMLEFT = new decimal[2] { (decimal)28.309217, (decimal)76.876831 };
        public static decimal[] BINGMAPS_GEO_FILTER_TOPRIGHT = new decimal[2] { (decimal)28.825425, (decimal)77.459106 };
    }
}
