﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace DpOgdiUI
{
    public class GenericVisualTreeHelper
    {
        public static FrameworkElement SearchFrameworkElement(FrameworkElement parentFrameworkElement, string childFrameworkElementNameToSearch)
        {
            FrameworkElement childFrameworkElementFound = null;
            SearchFrameworkElement(parentFrameworkElement, ref childFrameworkElementFound, childFrameworkElementNameToSearch);
            return childFrameworkElementFound;
        }

        public static List<FrameworkElement> GetAllChildFrameworkElement(FrameworkElement parentElement)
        {
            List<FrameworkElement> childFrameworkElementFound = new List<FrameworkElement>();
            SearchAllChildFrameworkElement(parentElement, childFrameworkElementFound);
            return childFrameworkElementFound;
        }

        private static void SearchFrameworkElement(FrameworkElement parentFrameworkElement, ref FrameworkElement childFrameworkElementToFind, string childFrameworkElementName)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parentFrameworkElement);
            if (childrenCount > 0)
            {
                FrameworkElement childFrameworkElement = null;
                for (int i = 0; i < childrenCount; i++)
                {
                    childFrameworkElement = (FrameworkElement)VisualTreeHelper.GetChild(parentFrameworkElement, i);
                    if (childFrameworkElement != null && childFrameworkElement.Name.Equals(childFrameworkElementName))
                    {
                        childFrameworkElementToFind = childFrameworkElement;
                        return;
                    }
                    SearchFrameworkElement(childFrameworkElement, ref childFrameworkElementToFind, childFrameworkElementName);
                }
            }
        }

        private static void SearchAllChildFrameworkElement(FrameworkElement parentFrameworkElement, List<FrameworkElement> allChildFrameworkElement)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parentFrameworkElement);
            if (childrenCount > 0)
            {
                for (int i = 0; i < childrenCount; i++)
                {
                    FrameworkElement childFrameworkElement = (FrameworkElement)VisualTreeHelper.GetChild(parentFrameworkElement, i);
                    allChildFrameworkElement.Add(childFrameworkElement);
                    SearchAllChildFrameworkElement(childFrameworkElement, allChildFrameworkElement);
                }
            }
        }
    }
}
