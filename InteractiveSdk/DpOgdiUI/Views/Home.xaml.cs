using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using DpOgdiUI.DelhiPolice;
using System.Data.Services.Client;
using Microsoft.Maps.MapControl;
using System.IO;
using System.Xml.Linq;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using DpOgdiUI.Controls;
using System.ComponentModel;
using DpOgdiUI.CitizenFeedback;
using System.Windows.Data;
using DpOgdiUI.GoogleMapsAPI;
using DpOgdiUI.Utilities;
using System.Text.RegularExpressions;
using Liquid;

namespace DpOgdiUI
{
    public partial class Home : Page
    {
        public DelhiPolice.delhiDataService context = null;
        public DataServiceCollection<CrimesItem> incidentsCollection = null;
        public DataServiceCollection<PoliceStationsItem> policeStations = null;
        public DataServiceCollection<OfficersItem> officersCollection = null;
        public Dictionary<string, PoliceStationData> mapData = new Dictionary<string, PoliceStationData>();
        string kml_url = AppConfig.MAP_KML_URL;
        //public String[] Crime_Types = new String[] { "ALL TYPES", "MURDER", "SNATCHING" };
        // private bool eventAdded = false;

        public String[] Categories = new String[] {
            "Gambling activity",
            "Illegal sale of Liquor",
            "Drinking in Public Place",
            "Nuisance at Public Place",
            "Encroachment on Road",
            "Encroachment in Market",
            "Road Accident Prone Location",
            "Information about wanted criminal",
            "Other Unlawful Activity"
        };
        //public List<CrimeData> crimeData = new List<CrimeData>();
        //public ContextMenu popupMenu;
        public Home()
        {
            InitializeComponent();

            crimeMap.Visibility = Visibility.Collapsed;

            Load_PoliceStations();

            var uri = new Uri(AppConfig.BLOB_URL + "man.jpg");
            SHOPhotoBox.Source = new BitmapImage(uri);

            SHOPhotoBox.ImageFailed += (s, e) =>
            {
                SHOPhotoBox.Source = new BitmapImage(new Uri(AppConfig.BLOB_URL + "man.jpg"));
            };
            crimeMap.MouseClick += new EventHandler<MapMouseEventArgs>(map_Clicked);
            crimeMap.MouseRightButtonDown += (s, e) => { e.Handled = true; };

            CrimeStatisticsGrid.RowBackground = new SolidColorBrush(Color.FromArgb(0xFF, 0xDF, 0xDF, 0xDF));
            CrimeStatisticsGrid.AlternatingRowBackground = new SolidColorBrush(Color.FromArgb(0xFF, 0xEE, 0xEE, 0xEE));

            feedbackCommentBox.VerticalContentAlignment = VerticalAlignment.Top;
            feedbackCommentBox.AcceptsReturn = true;

            feedbackCategoryList.ItemsSource = Categories;

            App.disclaimer.IsTimerEnabled = true;
            App.disclaimer.TimeUntilClose = new TimeSpan(0, 0, 8);

            App.disclaimer.Show();
            this.Loaded += new RoutedEventHandler(Home_Loaded);
        }

        public void Home_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Browser.HtmlPage.Plugin.Focus();
            locationBox.Focus();

        }
        public void Load_PoliceStationKml()
        {
            App.busyIndicator.BusyContent = "Loading geographic data ...";
            App.busyIndicator.IsBusy = true;
            WebClient kmlClient = new WebClient();
            kmlClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(PoliceStationKml_Completed);
            kmlClient.DownloadStringAsync(new Uri(kml_url));
        }
        public void Load_PoliceStations()
        {
            App.busyIndicator.BusyContent = "Loading police stations ...";
            App.busyIndicator.IsBusy = true;
            context = new delhiDataService(new Uri(AppConfig.DATASERVICE_URL));

            policeStations = new DataServiceCollection<PoliceStationsItem>(context);
            policeStations.LoadCompleted += new EventHandler<LoadCompletedEventArgs>(PoliceStations_Completed);
            var query = context.PoliceStations;
            policeStations.LoadAsync(query);
        }
        public void Load_PS_CrimeData(int psid)
        {
            App.busyIndicator.IsBusy = true;
            context = new delhiDataService(new Uri(AppConfig.DATASERVICE_URL));

            Load_PSCitizenReportcount(psid);

            incidentsCollection = new DataServiceCollection<CrimesItem>(context);
            incidentsCollection.LoadCompleted += new EventHandler<LoadCompletedEventArgs>(PS_CrimeData_Complete);

            var query = from c in context.Crimes
                        where c.ps_id == psid
                        select c;
            incidentsCollection.LoadAsync(query);
        }
        void Load_PSCitizenReportcount(int psid)
        {
            var client = new CitizenReportsClient();
            client.getCitizenReportCountByPoliceStationCompleted += new EventHandler<getCitizenReportCountByPoliceStationCompletedEventArgs>(Load_PSCitizenReportCountCompleted);
            client.getCitizenReportCountByPoliceStationAsync(psid);
        }
        void Load_OfficerData(string officerid)
        {
            context = new delhiDataService(new Uri(AppConfig.DATASERVICE_URL));
            officersCollection = new DataServiceCollection<OfficersItem>(context);
            officersCollection.LoadCompleted += new EventHandler<LoadCompletedEventArgs>(officersCollection_LoadCompleted);

            var query = (from c in context.Officers
                         where c.id == officerid
                         select c).Take(1);
            officersCollection.LoadAsync(query);
        }

        void officersCollection_LoadCompleted(object sender, LoadCompletedEventArgs e)
        {
            SHONameBox.Text = "";
            SHODesigBox.Text = "";

            if (e.Error == null)
            {
                var officer = officersCollection.FirstOrDefault();
                if (officer != null)
                {
                    if (!string.IsNullOrEmpty(officer.name))
                    {
                        SHONameBox.Text = "Insp. " + ToTitleCase(officer.name);
                        SHODesigBox.Text = "Station House Officer";
                    }
                }
            }
        }
        void Load_PSCitizenReportCountCompleted(object sender, getCitizenReportCountByPoliceStationCompletedEventArgs e)
        {
            // Commented since feedback is disabled

            //if (e.Error == null)
            //{
            //    CitizenReportBox.Text = e.Result.ToString();
            //    if (current_psname != null)
            //    {
            //        mapData[current_psname.ToUpper()].ReportCount = e.Result;        // Update crimeData
            //        //                    current_psname = null;
            //    }
            //}
            //else
            //{
            //    CitizenReportBox.Text = "0";
            //}
        }

        public void PoliceStations_Completed(object sender, LoadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                foreach (var ps in policeStations)
                {
                    var stn = new PoliceStationData();
                    stn.id = ps.id.Value;
                    stn.name = ps.stationname;
                    stn.stnData = ps;
                    if (ps.stationname != null)
                        mapData.Add(ps.stationname.ToUpper(), stn);
                }
                App.busyIndicator.IsBusy = false;
                Load_PoliceStationKml();
            }
            else
            {
                //MessageBox.Show("Error! : " + e.Error.Message);
                App.busyIndicator.IsBusy = false;
            }
        }
        private LocationCollection points = null;
        public void PoliceStationKml_Completed(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                XDocument doc = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + e.Result);
                XNamespace ns = XNamespace.Get("http://www.opengis.net/kml/2.2");

                var placemarks = from p in doc.Descendants(ns + "Placemark")
                                 select p;
                ClearMap();
                points = new LocationCollection(); // For Auto Zoom

                var cnt = 0;

                foreach (var placemark in placemarks)
                {
                    var p = placemark;
                    var Name = p.Element(ns + "name").Value;
                    var Coords = p.Descendants(ns + "coordinates").FirstOrDefault();
                    if (Coords != null)
                    {
                        var coordStr = Coords.Value;
                        var coords = CoordinateCollection.Parse(coordStr);

                        if (coords.Count != 0)
                        {
                            var mapPoly = new MapPolygon();
                            var locs = new LocationCollection();

                            double lat_sum = 0;
                            double lon_sum = 0;

                            foreach (var point in coords)
                            {
                                locs.Add(new Microsoft.Maps.MapControl.Location(point.Latitude, point.Longitude));
                                points.Add(new Microsoft.Maps.MapControl.Location(point.Latitude, point.Longitude));

                                lat_sum += point.Latitude;
                                lon_sum += point.Longitude;
                            }
                            lat_sum = lat_sum / coords.Count;
                            lon_sum = lon_sum / coords.Count;

                            var mapCenter = new Location(lat_sum, lon_sum);

                            double min = Double.MaxValue;
                            // Calculate maximum in-radius = min distance from center to any point
                            foreach (var point in coords)
                            {
                                var dist = Distance(point, mapCenter);
                                if (dist < min)
                                {
                                    min = dist;
                                }
                            }
                            mapPoly.Opacity = AppConfig.MAP_TILE_OPACITY;
                            mapPoly.Fill = new SolidColorBrush(AppConfig.MAP_TILE_FILL);
                            mapPoly.Stroke = new SolidColorBrush(AppConfig.MAP_TILE_STROKE);
                            mapPoly.StrokeThickness = AppConfig.MAP_TILE_STROKE_THICKNESS;
                            mapPoly.Locations = locs;
                            mapPoly.Name = Name; // Set Police station name to poly element
                            mapPoly.MouseEnter += new MouseEventHandler(mapPoly_MouseEnter);
                            mapPoly.MouseLeave += new MouseEventHandler(mapPoly_MouseLeave);

                            ToolTipService.SetToolTip(mapPoly, "Police Station " + Name);

                            TextBlock tb = new TextBlock
                            {
                                //    Text = Name
                            };

                            // set the position of the textblock and add it to a new map layer
                            MapLayer.SetPositionOrigin(tb, PositionOrigin.Center);
                            MapLayer.SetPosition(tb, mapCenter);
                            tb.IsHitTestVisible = false;

                            if (!mapData.ContainsKey(Name.ToUpper()))
                                continue;

                            mapData[Name.ToUpper()].center = mapCenter;
                            mapData[Name.ToUpper()].maxInRadius = min;
                            mapData[Name.ToUpper()].polygon = mapPoly;

                            var stn = mapData[Name.ToUpper()].stnData;
                            //if (stn.latitude.HasValue && stn.longitude.HasValue && pointInPolygon(mapPoly.Locations, stn.latitude.Value, stn.longitude.Value))
                            if (stn.latitude.HasValue && stn.longitude.HasValue)    // Removed filter
                            {
                                var pin = new Pushpin();
                                pin.Location = new Location(stn.latitude.Value, stn.longitude.Value);
                                //pin.Template = Application.Current.Resources["CrimePushPinTemplate"] as ControlTemplate;
                                pin.Visibility = Visibility.Visible;


                                ToolTipService.SetToolTip(pin, "Police Station " + stn.stationname);
                                pin.Template = Application.Current.Resources["StationPushPinTemplate"] as ControlTemplate;

                                StationPinLayer.AddChild(pin, pin.Location);
                                mapData[Name.ToUpper()].stnpoint = pin;
                                cnt++;
                            }
                            polyLayer.Children.Add(mapPoly);
                            polyLayer.Children.Add(tb);

                            mapPoly.MouseLeftButtonUp += new MouseButtonEventHandler(mapPoly_Clicked);
                            mapPoly.MouseRightButtonDown += new MouseButtonEventHandler(mapPoly_FeedbackForm);
                        }
                    }
                }
                AutoZoomMap(crimeMap, points);
                crimeMap.Visibility = Visibility.Visible;
                clearBtn_Click(sender, null);
            }
            App.busyIndicator.IsBusy = false;
            locationBox.Focus();
            //Load_Incidents("ALL TYPES");
        }
        public void PS_CrimeData_Complete(object sender, LoadCompletedEventArgs e)
        {
            // Map the codes for different crime types
            if (e.Error == null)
            {
                var incidents = sender as DataServiceCollection<CrimesItem>;
                var crimeData = new List<CrimeData>();
                var hashmap = new Dictionary<string, int>();

                if (incidents.Count > 0)
                {
                    var min_year = Int32.MaxValue;
                    var max_year = Int32.MinValue;

                    int total_theft = 0, auto_theft = 0, total_ipc = 0, servant_theft = 0;
                    foreach (var item in incidents)
                    {
                        var label = ToTitleCase(item.crime_head).Replace("Ipc", "IPC");

                        var incident_year = item.date.Value.Year;

                        if (incident_year < min_year) min_year = incident_year;
                        if (incident_year > max_year) max_year = incident_year;

                        if (label.Equals("Total Theft"))
                        {
                            total_theft = item.incident_count.Value;
                        }
                        else if (label.Equals("M.V.Theft"))
                        {
                            auto_theft = item.incident_count.Value;
                        }
                        else if (label.Equals("Servant Theft"))
                        {
                            servant_theft = item.incident_count.Value;
                        }
                        else if (label.Equals("Total IPC"))
                        {
                            total_ipc = item.incident_count.Value;
                        }
                        else
                        {
                            crimeData.Add(new CrimeData { Type = label, Count = item.incident_count.Value.ToString() });
                        }
                    }

                    if (auto_theft > 0)
                    {
                        crimeData.Add(new CrimeData { Type = "Auto Theft", Count = auto_theft.ToString() });
                    }
                    if (servant_theft > 0)
                    {
                        crimeData.Add(new CrimeData { Type = "Servant Theft", Count = servant_theft.ToString() });
                    }
                    if (total_theft > (auto_theft + servant_theft))
                    {
                        var other_theft = total_theft - auto_theft - servant_theft;
                        crimeData.Add(new CrimeData { Type = "Other Theft", Count = other_theft.ToString() });
                    }
                    if (total_ipc > 0)
                    {
                        crimeData.Add(new CrimeData { Type = "Total IPC", Count = total_ipc.ToString() });
                    }
                    if (min_year == max_year)
                    {
                        CrimeStatisticsTitle.Text = "Crime Statistics ( " + min_year + " )";
                    }
                    else
                    {
                        CrimeStatisticsTitle.Text = "Crime Statistics ( " + min_year + " , " + max_year + ")";
                    }
                    if (current_psname != null)
                    {
                        mapData[current_psname.ToUpper()].crimeData = crimeData;        // Update crimeData
                    }
                }
                else
                {
                    crimeData.Add(new CrimeData { Type = "Murder", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Att. To Murder", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Robbery", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Rape", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Snatching", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Hurt", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Burglary", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "M.V.Theft", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Other Theft", Count = "0" });
                    crimeData.Add(new CrimeData { Type = "Total IPC", Count = "0" });
                    CrimeStatisticsTitle.Text = "Crime Statistics";
                }
                CrimeStatisticsGrid.ItemsSource = null;
                CrimeStatisticsGrid.ItemsSource = crimeData;
            }
            else
            {
                var crimeData = new List<CrimeData>();
                crimeData.Add(new CrimeData { Type = "Murder", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Att. To Murder", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Robbery", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Rape", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Snatching", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Hurt", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Burglary", Count = "0" });
                crimeData.Add(new CrimeData { Type = "M.V.Theft", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Other Theft", Count = "0" });
                crimeData.Add(new CrimeData { Type = "Total IPC", Count = "0" });

                CrimeStatisticsTitle.Text = "Crime Statistics";
                CrimeStatisticsGrid.ItemsSource = null;
                CrimeStatisticsGrid.ItemsSource = crimeData;
                //MessageBox.Show(e.Error.Message);
            }
            App.busyIndicator.IsBusy = false;
        }
        public string current_psname = null;
        public void map_Clicked(object sender, MapMouseEventArgs e)
        {
            HideDetailsPane();
        }
        private DraggablePushpin dPin = new DraggablePushpin();
        public void mapPoly_FeedbackForm(object sender, MouseButtonEventArgs e)
        {
            Location mylocation = crimeMap.ViewportPointToLocation(e.GetPosition(crimeMap));

            var poly = sender as MapPolygon;
            poly.Opacity = AppConfig.MAP_TILE_OPACITY;
            GiveFeedback(poly.Name, mylocation);
            e.Handled = true;
        }
        public void GiveFeedback(string psname, Location myLocation)
        {
            feedbackLocation = myLocation;
            doReverseGeocode(myLocation);

            dPin.Background = new SolidColorBrush(AppConfig.FEEDBACK_PIN_COLOR);
            dPin.Location = feedbackLocation;
            dPin.PinDropped += new DraggablePushpin.PinDroppedHandler(dPin_PinDropped);
            // dPin.MouseLeftButtonUp += new MouseButtonEventHandler(dPin_MouseLeftButtonUp);
            // dPin.DragLeave += new DragEventHandler(dPin_DragLeave);
            ToolTipService.SetToolTip(dPin, "Drag this pin to set location");

            PushPinLayer.Children.Clear();
            PushPinLayer.Children.Add(dPin);

            HideSearchResults();
            HideDetailsPane();
            ShowFeedbackForm();

            current_psname = psname;
            PoliceStationData stn;
            stn = mapData[psname.ToUpper()];
            if (stn != null)
            {
                feedbackStationBox.Text = stn.name;
            }
            else
            {
                feedbackStationBox.Text = "";
            }
            feedbackCommentBox.ClearSelection();
            feedbackCommentBox.HTML = "";
            feedbackCommentBox.Text = "";

            feedbackNameBox.Focus();
        }

        void dPin_PinDropped(object sender, EventArgs e)
        {
            var pin = (DraggablePushpin)sender;
            var mylocation = pin.Location;
            feedbackLocation = mylocation;
            MapPolygon poly = null;
            foreach (var stn in mapData)
            {
                if (stn.Value.polygon != null)
                {
                    if (pointInPolygon(stn.Value.polygon.Locations, mylocation.Latitude, mylocation.Longitude)) // If point is within the current station radius
                    {
                        poly = stn.Value.polygon;
                        break;
                    }
                }
            }
            if (poly != null)
            {
                poly.Opacity = 0.5;
                doReverseGeocode(mylocation);
                feedbackStationBox.Text = poly.Name;
            }
            else
            {
                HideFeedbackForm();
            }

        }
        void doReverseGeocode(Location mylocation)
        {
            var client = new GoogleMapsClient();
            var pos = new GeographicPosition() { Latitude = (decimal)mylocation.Latitude, Longitude = (decimal)mylocation.Longitude };
            client.ReverseGeocodeCompleted += new EventHandler<ReverseGeocodeCompletedEventArgs>(client_ReverseGeocodeCompleted);
            client.ReverseGeocodeAsync(pos);
        }
        void client_ReverseGeocodeCompleted(object sender, ReverseGeocodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Results.Count > 0)
                {
                    var res = e.Result.Results[0];
                    feedbackPlaceBox.Text = res.FormattedAddress;
                }
            }
            else
            {
                feedbackPlaceBox.Text = "";
            }
        }

        private Location feedbackLocation = null;
        public MapPolygon selectedPolygon = null;
        public void mapPoly_Clicked(object sender, MouseButtonEventArgs e)
        {
            HideFeedbackForm();
            HideSearchResults();
            ClearMap(false);

            Point p = e.GetPosition(PushPinLayer);

            var poly = sender as MapPolygon;
            poly.Opacity = 0.8;
            if (selectedPolygon != null)
            {
                selectedPolygon.Opacity = AppConfig.MAP_TILE_OPACITY;
            }
            selectedPolygon = poly;
            PoliceStationData stn;

            if (!mapData.Keys.Contains(poly.Name.ToUpper())) return; // Sanity Check

            stn = mapData[poly.Name.ToUpper()];

            if (stn.stnData == null || stn.polygon == null) return; // Sanity Check

            crimeMap.Center = stn.center;

            current_psname = poly.Name;
            /*
            if (!string.IsNullOrEmpty(stn.stnData.shoname))
            {
                SHONameBox.Text = "Insp. " + ToTitleCase(stn.stnData.shoname);
                SHODesigBox.Text = "Station House Officer";
            }
            else
            {
                SHONameBox.Text = "";
                SHODesigBox.Text = "";
            }
            */
            SHONameBox.Text = "";
            SHODesigBox.Text = "";
            Load_OfficerData(stn.stnData.shoid);

            AutoZoomMap(crimeMap, stn.polygon.Locations);
            var phn = stn.stnData.shophone;
            if (phn != null)
            {
                var numbers = phn.Split(new char[] { ',' });
                SHOPhoneBox.Text = "011 " + numbers[0];

                if (numbers.Count() > 1)
                {
                    SHOPhoneBox.Text += " / 011 " + numbers[1];
                }
                SHOMobileBox.Text = stn.stnData.shomobile;
                // SHOEmailBox.Text = stn.stnData.shoemail;
                SHOEmailBox.Content = stn.stnData.shoemail;
                //SHOEmailBox.NavigateUri = new Uri("mailto:"+stn.stnData.shoemail);
                if (string.IsNullOrEmpty(stn.stnData.shoemail))
                {
                    EmailLabelBox.Visibility = Visibility.Collapsed;
                }
                else
                {
                    SHOEmailBox.NavigateUri = new Uri("mailto://" + stn.stnData.shoemail, UriKind.Absolute);
                    EmailLabelBox.Visibility = Visibility.Visible;
                }
            }
            else
            {
                SHOPhoneBox.Text = "";
                // SHOPhoneBox02.Text = "";
                SHOMobileBox.Text = "";
                // SHOEmailBox.Text = "";
                SHOEmailBox.Content = "";
                SHOEmailBox.NavigateUri = null;
                EmailLabelBox.Visibility = Visibility.Collapsed;
            }
            if (string.IsNullOrEmpty(stn.stnData.address))
            {
                StationAddress.Visibility = Visibility.Collapsed;
            }
            else
            {
                StationAddress.Text = stn.stnData.address;
                StationAddress.Visibility = Visibility.Visible;
            }
            Uri uri = null;
            if (!string.IsNullOrEmpty(stn.stnData.shoid))
            {
                uri = new Uri(AppConfig.BLOB_URL + stn.stnData.shoid + ".jpg");
            }
            else
            {
                uri = new Uri(AppConfig.BLOB_URL + "man.jpg");
            }
            SHOPhotoBox.Source = new BitmapImage(uri);

            StationTitle.Text = stn.name;
            if (stn.crimeData == null)
            {
                App.busyIndicator.BusyContent = "Loading Crime Statistics ...";
                App.busyIndicator.IsBusy = true;
                current_psname = stn.name;
                Load_PS_CrimeData(stn.id);
            }
            else
            {
                Load_PSCitizenReportcount(stn.id);
                CrimeStatisticsGrid.ItemsSource = null;
                CrimeStatisticsGrid.ItemsSource = stn.crimeData;
            }

            ShowDetailsPane();
            e.Handled = true;

        }
        private void ShowFeedbackForm()
        {
            if (FeedbackPane.Width > 0)
            {
                feedbackFadeInOut.Begin();
            }
            else
            {
                feedbackSlideIn.Begin();
            }
        }
        private void HideFeedbackForm()
        {
            if (FeedbackPane.Width > 0)
            {
                feedbackSlideOut.Begin();
                PushPinLayer.Children.Clear();
            }
        }
        private void ShowDetailsPane()
        {
            //MaskMap();
            if (DetailsPane.Width != 0)
            {
                fadeInOut.Begin();
            }
            else
            {
                slideIn.Begin();
            }
            DetailScroller.ScrollToTop();
        }
        private void HideDetailsPane()
        {
            //UnmaskMap();
            if (DetailsPane.Width > 0)
            {
                slideOut.Begin();
            }
        }
        private void ShowSearchResults()
        {
            if (searchResults != null)
            {
                SearchResultsPane.Visibility = Visibility.Visible;
                ResultList.SelectedItem = null;
            }
        }
        private void HideSearchResults()
        {
            if (searchResults != null)
            {
                SearchResultsPane.Visibility = Visibility.Collapsed;
            }
        }
        private void MaskMap()
        {
            crimeMap.Opacity = 0.4;
            //OverlayMask.Visibility = Visibility.Visible;
        }
        private void UnmaskMap()
        {
            crimeMap.Opacity = 1.0;
            //OverlayMask.Visibility = Visibility.Collapsed;
        }
        private void AutoZoomMap(Map map, LocationCollection points)
        {
            if (points.Count > 0)
            {
                var x = points.OrderBy(p => p.Latitude).OrderBy(p => p.Longitude);
                x = x.OrderBy(p => p.Longitude).OrderBy(p => p.Latitude);

                var viewRect = new LocationRect(x.First(), x.Last());
                map.SetView(viewRect);
            }
        }
        public void ClearMap(bool clearRoute = true)
        {
            List<UIElement> pushpinToRemove = new List<UIElement>();
            foreach (UIElement p in PushPinLayer.Children)
            {
                if (p.GetType() == typeof(Pushpin))
                {
                    pushpinToRemove.Add(p);
                }
            }

            /*
            foreach (UIElement p in ResultPushPinLayer.Children)
            {
                if (p.GetType() == typeof(Pushpin))
                {
                    pushpinToRemove.Add(p);
                }
            }*/
            /*
            foreach (UIElement p in StationPinLayer.Children)
            {
                if (p.GetType() == typeof(Pushpin))
                {
                    pushpinToRemove.Add(p);
                }
            }*/
            foreach (Pushpin pin in pushpinToRemove)
            {
                //PushPinLayer.Children.Remove(pin);
                pin.Visibility = Visibility.Collapsed;
            }
            if (clearRoute)
            {
                ResultPushPinLayer.Children.Clear();
                myRouteLayer.Children.Clear();
            }
        }
        public void ResetPins()
        {
            List<UIElement> pushpinToRemove = new List<UIElement>();
            foreach (UIElement p in PushPinLayer.Children)
            {
                if (p.GetType() == typeof(Pushpin))
                {
                    pushpinToRemove.Add(p);
                }
            }
            foreach (Pushpin pin in pushpinToRemove)
            {
                //PushPinLayer.Children.Remove(pin);
                pin.Visibility = Visibility.Visible;
            }
        }
        public void ResetMap()
        {
        }
        public string GetPoliceStationName(int id)
        {
            var name = from p in policeStations where p.id == id select p.stationname;
            return name.FirstOrDefault();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void goBtn_Click(object sender, RoutedEventArgs e)
        {
            HideDetailsPane();

            SearchResultsPane.Visibility = Visibility.Collapsed;
            FeedbackPane.Width = 0;
            PushPinLayer.Children.Clear();

            App.busyIndicator.BusyContent = "Searching ...";
            App.busyIndicator.IsBusy = true;
            if (locationBox.Text.Length == 0) { MessageBox.Show("Please enter a valid location to search"); return; }
            searchResults = null;
            ResultList.ItemsSource = null;
            //do_geoCode(locationBox.Text+", Delhi");

            var searchText = locationBox.Text;
            if((searchText.ToLower().Contains("mandaoli") || searchText.ToLower().Contains("mandawali") 
                || searchText.ToLower().Contains("mandavali")
                ) && !searchText.Contains(","))
            {
                searchText = searchText+", Mandaoli";
            }
            do_geoCode(searchText);
        }
        private void do_geoCode(string query)
        {
            if (query != "")
            {
                var client = new GoogleMapsClient();
                var req = new GeocodingRequest();
                req.Address = query;
                client.DoRequestCompleted += new EventHandler<DoRequestCompletedEventArgs>(geocodeService_GeocodeCompleted);
                client.DoRequestAsync(req);
            }
        }
        /*
        private void done(object sender, GetHelloCompletedEventArgs e)
        {
            MessageBox.Show(e.Result);
            App.busyIndicator.IsBusy = false;
        }
        */
        ObservableCollection<GeocodingResult> searchResults = null;

        private ObservableCollection<GeocodingResult> filterGeocodeResults(GeocodingResponse geocodeResponse)
        {
            var res = geocodeResponse.Results.FirstOrDefault();
            var output = new ObservableCollection<GeocodingResult>();

            // filter out results outside our limits
            var bl = AppConfig.BINGMAPS_GEO_FILTER_BOTTOMLEFT;// new decimal[2] { (decimal)28.309217, (decimal)76.876831 };// ,
            var tr = AppConfig.BINGMAPS_GEO_FILTER_TOPRIGHT; // new decimal[2] { (decimal)28.825425, (decimal)77.459106 };

            foreach (var result in geocodeResponse.Results)
            {

                if (result.Geometry.Location.Latitude > bl[0] && result.Geometry.Location.Longitude > bl[1]
                    && result.Geometry.Location.Latitude < tr[0] && result.Geometry.Location.Longitude < tr[1])
                {
                    if (result.FormattedAddress.Length > 50)
                    {
                        result.FormattedAddress = result.FormattedAddress.Substring(0, 50) + " ...";
                    }
                    output.Add(result);
                }
            }
            return output;
        }
        private void geocodeService_GeocodeCompleted(object sender, DoRequestCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                var results = filterGeocodeResults(e.Result);

                if (results.Count > 0)
                {
                    ClearMap();

                    searchResults = results;
                    ResultList.ItemsSource = results;
                    ResultList.DisplayMemberPath = "FormattedAddress";

                    if (results.Count > 0)
                    {
                        ShowSearchResults();
                    }
                }
                else
                {
                    MessageBox.Show("No results found");
                }
            }
            else
            {
            }
            App.busyIndicator.IsBusy = false;
        }

        private void do_routeSearch(Location location, Location location_2)
        {
            App.busyIndicator.BusyContent = "Finding route ... ";
            App.busyIndicator.IsBusy = true;

            var client = new GoogleMapsClient();
            var from = location.Latitude + "," + location.Longitude;
            var to = location_2.Latitude + "," + location_2.Longitude;
            client.GetDirectionsCompleted += routeService_Completed;
            client.GetDirectionsAsync(from, to);
        }
        private void routeService_Completed(object sender, GetDirectionsCompletedEventArgs e)
        // private void routeService_Completed(object sender, CalculateRouteCompletedEventArgs e)
        {
            /*
            // If the route calculate was a success and contains a route, then draw the route on the map.
            if ((e.Result.ResponseSummary.StatusCode == RouteService.ResponseStatusCode.Success) & (e.Result.Result.Legs.Count != 0))
            {*/
            if (e.Error == null && e.Result.Status == ServiceResponseStatus.Ok)
            {
                // Set properties of the route line you want to draw.
                myRouteLayer.Children.Clear();

                Color routeColor = Colors.Yellow;
                SolidColorBrush routeBrush = new SolidColorBrush(AppConfig.MAP_ROUTELINE_COLOR);
                MapPolyline routeLine = new MapPolyline();
                routeLine.Locations = new LocationCollection();
                routeLine.Stroke = routeBrush;
                routeLine.Opacity = AppConfig.MAP_ROUTELINE_OPACITY;
                routeLine.StrokeThickness = AppConfig.MAP_ROUTELINE_THICKNESS;

                // Retrieve the route points that define the shape of the route.
                foreach (var p in e.Result.Routes[0].point.points)
                {
                    //routeLine.Locations.Add(new Location(p.Latitude, p.Longitude));
                    routeLine.Locations.Add(new Location((double)p.Latitude, (double)p.Longitude));
                }

                // Add a map layer in which to draw the route.
                //MapLayer myRouteLayer = new MapLayer();
                //crimeMap.Children.Add(myRouteLayer);

                // Add the route line to the new layer.
                myRouteLayer.Children.Add(routeLine);

                // Figure the rectangle which encompasses the route. This is used later to set the map view.
                LocationRect rect = new LocationRect(routeLine.Locations[0], routeLine.Locations[routeLine.Locations.Count - 1]);

                // Set the map view using the rectangle which bounds the rendered route.
                crimeMap.SetView(rect);
            }
            App.busyIndicator.IsBusy = false;
        }
        public string ToTitleCase(string str)
        {
            return Regex.Replace(str, @"\w+", (m) =>
            {
                string tmp = m.Value;
                return char.ToUpper(tmp[0]) + tmp.Substring(1, tmp.Length - 1).ToLower();
            });
        }

        static bool pointInPolygon(LocationCollection points, double lat, double lon)
        {
            int i;
            int j = points.Count - 1;
            bool inPoly = false;

            for (i = 0; i < points.Count; i++)
            {
                if (points[i].Longitude < lon && points[j].Longitude >= lon
                  || points[j].Longitude < lon && points[i].Longitude >= lon)
                {
                    if (points[i].Latitude + (lon - points[i].Longitude) /
                      (points[j].Longitude - points[i].Longitude) * (points[j].Latitude
                        - points[i].Latitude) < lat)
                    {
                        inPoly = !inPoly;
                    }
                }
                j = i;
            }
            return inPoly;
        }

        private double ToRad(double d)
        {
            return d * (Math.PI / 180.0);
        }
        private double ToDeg(double r)
        {
            return r * (180.0 / Math.PI);
        }
        private double Distance(Vector point1, Location point2)
        {
            double dLat = ToRad(point2.Latitude - point1.Latitude);
            double dLon = ToRad(point2.Longitude - point1.Longitude);

            double a = Math.Pow(Math.Sin(dLat / 2), 2) +
                       Math.Cos(ToRad(point1.Latitude)) * Math.Cos(ToRad(point2.Latitude)) *
                       Math.Pow(Math.Sin(dLon / 2), 2);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return ToDeg(c);
        }

        public void mapPoly_MouseEnter(object sender, MouseEventArgs e)
        {
            var mapPoly = (sender as MapPolygon);
            mapPoly.Opacity = AppConfig.MAP_TILE_HOVER_OPACITY;
            mapPoly.Fill = new SolidColorBrush(AppConfig.MAP_TILE_HOVER_FILL);
            mapPoly.Stroke = new SolidColorBrush(AppConfig.MAP_TILE_HOVER_STROKE);
            mapPoly.StrokeThickness = AppConfig.MAP_TILE_HOVER_STROKE_THICKNESS;
        }
        public void mapPoly_MouseLeave(object sender, MouseEventArgs e)
        {
            var mapPoly = (sender as MapPolygon);
            mapPoly.Opacity = AppConfig.MAP_TILE_OPACITY;
            mapPoly.Fill = new SolidColorBrush(AppConfig.MAP_TILE_FILL);
            mapPoly.Stroke = new SolidColorBrush(AppConfig.MAP_TILE_STROKE);
            mapPoly.StrokeThickness = AppConfig.MAP_TILE_STROKE_THICKNESS;
        }

        private void clearBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearMap();
            searchResults = null;
            ResultList.ItemsSource = null;
            SearchResultsPane.Visibility = Visibility.Collapsed;
            locationBox.Text = "";

            myRouteLayer.Children.Clear();
            foreach (var stn in mapData)
            {
                if (stn.Value.polygon != null)
                {
                    stn.Value.polygon.Visibility = Visibility.Visible;
                }
                if (stn.Value.stnpoint != null)
                {
                    stn.Value.stnpoint.Visibility = Visibility.Visible;
                }
            }
            ResultPushPinLayer.Children.Clear();
            AutoZoomMap(crimeMap, points);
            if (crimeMap.ZoomLevel != AppConfig.MAP_DEFAULT_ZOOMLEVEL && crimeMap.TargetZoomLevel != AppConfig.MAP_DEFAULT_ZOOMLEVEL)
                crimeMap.ZoomLevel = AppConfig.MAP_DEFAULT_ZOOMLEVEL;

            HideDetailsPane();
            HideFeedbackForm();
            HideSearchResults();

            locationBox.Focus();
        }

        private void btnDetailPanelClose_Click(object sender, RoutedEventArgs e)
        {
            HideDetailsPane();
            ShowSearchResults();
        }

        private void btnFeedbackPanelClose_Click(object sender, RoutedEventArgs e)
        {
            //feedbackSlideOut.Begin();
            HideFeedbackForm();
            ShowSearchResults();
        }

        private void btnFeedbackSubmit_Click(object sender, RoutedEventArgs e)
        {
            // Validate user details

            if (!string.IsNullOrEmpty(feedbackEmailBox.Text) && !RegexUtilities.IsValidEmail(feedbackEmailBox.Text))
            {
                MessageBox.Show("Please enter a valid email address or leave it blank");
                feedbackEmailBox.Focus();
                return;
            }
            if (feedbackCategoryList.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category");
                feedbackCategoryList.Focus();
                return;
            }
            /*if (string.IsNullOrEmpty(feedbackNameBox.Text))
            {
                MessageBox.Show("Please enter your name");
                feedbackNameBox.Focus();
                return;
            }*/
            if (!string.IsNullOrEmpty(feedbackPhoneBox.Text) && !RegexUtilities.IsValidPhone(feedbackPhoneBox.Text))
            {
                MessageBox.Show("Please enter a valid phone number or leave it blank");
                feedbackPhoneBox.Focus();
                return;
            }

            feedbackCommentBox.SelectAll();

            if (feedbackCommentBox.SelectedText.Length == 0)
            //if (feedbackCommentBox.Blocks.Count == 0)
            {
                MessageBox.Show("Please enter your feedback comment");
                feedbackCommentBox.Focus();
                return;
            }

            App.busyIndicator.BusyContent = "Sending feedback ...";
            App.busyIndicator.IsBusy = true;

            var client = new CitizenReportsClient();

            var reportObj = new ReportObject();
            reportObj.date_time = DateTime.UtcNow;
            reportObj.category = feedbackCategoryList.SelectedItem.ToString();

            feedbackLocation = dPin.Location;

            reportObj.latitude = feedbackLocation.Latitude;
            reportObj.longitude = feedbackLocation.Longitude;
            reportObj.name = feedbackNameBox.Text;
            feedbackCommentBox.SelectAll();
            // reportObj.details = feedbackCommentBox.Selection.Text;
            reportObj.details = feedbackCommentBox.SelectedText;

            reportObj.email = feedbackEmailBox.Text;
            reportObj.policestation = current_psname;
            reportObj.approval_stat = false;
            reportObj.phone = feedbackPhoneBox.Text;
            reportObj.location_name = feedbackPlaceBox.Text;

            var stn = mapData[current_psname.ToUpper()];

            reportObj.ps_id = stn.id;
            client.postCitizenReportAsync(reportObj);
            client.postCitizenReportCompleted += new EventHandler<postCitizenReportCompletedEventArgs>(feedback_Done);
        }
        public void feedback_Done(object sender, postCitizenReportCompletedEventArgs e)
        {
            App.busyIndicator.IsBusy = false;

            if (e.Error == null)
            {
                var res = e.Result;
                switch (res)
                {
                    case SubmissionStatus.SUCCESS:
                        MessageBox.Show("Your feedback report was submitted for review. Thank you!");
                        break;
                    case SubmissionStatus.IPCHECK:
                        MessageBox.Show("You have already submitted a feedback for this station in the past 15 minutes.\nPlease try later.");
                        break;
                    case SubmissionStatus.LIMIT:
                        MessageBox.Show("You have reached your daily limit. Please try later.");
                        break;
                    case SubmissionStatus.DUPLICATE:
                        MessageBox.Show("A duplicate submission was detected.");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Your feedback report was submitted for review. Thank you!");
            }
            // Clear the form
            feedbackEmailBox.Text = "";
            feedbackNameBox.Text = "";
            feedbackPhoneBox.Text = "";
            feedbackStationBox.Text = "";

            feedbackCommentBox.HTML = "";
            feedbackCommentBox.Text = "";

            //feedbackCommentBox.Blocks.Clear();
            //feedbackCommentBox.Clear();

            PushPinLayer.Children.Clear();
            feedbackSlideOut.Begin();
        }

        private void ResultList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = ResultList.SelectedIndex;
            if (searchResults != null && index != -1 && searchResults.Count > index)
            {
                var result = searchResults[index];

                var lat = (double)result.Geometry.Location.Latitude;
                var lon = (double)result.Geometry.Location.Longitude;

                bool found = false;
                PoliceStationData foundStn = null;

                foreach (var stn in mapData)
                {
                    if (stn.Value.polygon != null)
                    {
                        if (!found && pointInPolygon(stn.Value.polygon.Locations, lat, lon)) // If point is within the current station radius
                        {
                            stn.Value.polygon.Visibility = Visibility.Visible;
                            if (stn.Value.stnpoint != null)
                            {
                                stn.Value.stnpoint.Visibility = Visibility.Visible;
                                do_routeSearch(new Location(lat, lon), stn.Value.stnpoint.Location);
                            }
                            foundStn = stn.Value;
                            AutoZoomMap(crimeMap, stn.Value.polygon.Locations);
                            found = true;
                        }
                        else
                        {
                            if (stn.Value.stnpoint != null)
                            {
                                stn.Value.stnpoint.Visibility = Visibility.Visible;
                            }
                            //    stn.Value.polygon.Visibility = Visibility.Collapsed;
                        }
                    }

                }
                ResultPushPinLayer.Children.Clear();

                var pin = new Pushpin();
                pin.Template = Application.Current.Resources["ResultPushPinTemplate"] as ControlTemplate;
                pin.Location = new Location(lat, lon);
                ResultPushPinLayer.AddChild(pin, pin.Location, PositionOrigin.BottomCenter);

                ResultPushPinLayer.AddChild(new Label
                {
                    Content = AppConfig.SEARCH_RESULT_PIN_STRING,
                    FontWeight = FontWeights.Bold,
                }, pin.Location);


                if (!found) crimeMap.Center = pin.Location;

                if (found && foundStn.stnData.latitude.HasValue && foundStn.stnData.longitude.HasValue)
                {
                    ResultPushPinLayer.AddChild(new Label
                    {
                        Content = string.Format(AppConfig.STATION_PIN_STRING, foundStn.stnData.stationname),
                        Background = new SolidColorBrush(Colors.White),
                        FontWeight = FontWeights.Bold
                    }, new Location(foundStn.stnData.latitude.Value, foundStn.stnData.longitude.Value), PositionOrigin.TopCenter);

                    if (foundStn.stnpoint != null)
                    {
                        foundStn.stnpoint.Visibility = Visibility.Collapsed;
                    }
                    var stnpin = new Pushpin();
                    stnpin.Width = 50;
                    stnpin.Height = 50;
                    stnpin.Template = Application.Current.Resources["DestPushPinTemplate"] as ControlTemplate;
                    stnpin.Location = foundStn.stnpoint.Location;
                    ResultPushPinLayer.AddChild(stnpin, stnpin.Location, PositionOrigin.BottomCenter);
                }

            }
        }

        private void locationBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                goBtn_Click(sender, e);
            }
        }

        private void CrimeStatisticsGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.FontSize = 12;
            e.Row.BorderThickness = new Thickness(2, 2, 2, 2);
            e.Row.BorderBrush = new SolidColorBrush(Colors.White);
        }

        private void CrimeStatisticsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CrimeStatisticsGrid.SelectedItem = null;
        }

        private void btnSearchResultsClose_Click(object sender, RoutedEventArgs e)
        {
            HideSearchResults();
        }

        private void locationBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(locationBox.Text) && OverlayText.Visibility == Visibility.Collapsed)
            {
                OverlayText.Visibility = Visibility.Visible;
            }
            else if (OverlayText.Visibility == Visibility.Visible)
            {
                OverlayText.Visibility = Visibility.Collapsed;
            }
        }

        private void btnGiveFeedback_Click(object sender, RoutedEventArgs e)
        {
            Location mylocation = crimeMap.Center;
            GiveFeedback(current_psname, mylocation);
        }
    }
    public class PoliceStationData
    {
        public int id { get; set; }
        public Location center { get; set; }
        public MapPolygon polygon { get; set; }
        public double maxInRadius { get; set; }
        public string name { get; set; }
        public PoliceStationsItem stnData { get; set; }
        public DataServiceCollection<CrimesItem> incidents { get; set; }
        public List<CrimeData> crimeData;
        public int ReportCount { get; set; }
        public Pushpin stnpoint { get; set; }
    }
    public class CrimeData
    {
        public string Type { get; set; }
        public string Count { get; set; }
        //        public string CitizenReports { get; set; }
    }
    public class StationCrimeStatistics
    {
        public string cSnatching { get; set; }
        public string cMurder { get; set; }
        public string cRobbery { get; set; }
        public string cRape { get; set; }
    }
}