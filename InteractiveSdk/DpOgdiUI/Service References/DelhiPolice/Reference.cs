//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Original file name:
// Generation date: 18-Jan-12 2:05:53 AM
namespace DpOgdiUI.DelhiPolice
{
    
    /// <summary>
    /// There are no comments for delhiDataService in the schema.
    /// </summary>
    public partial class delhiDataService : global::System.Data.Services.Client.DataServiceContext
    {
        /// <summary>
        /// Initialize a new delhiDataService object.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public delhiDataService(global::System.Uri serviceRoot) : 
                base(serviceRoot)
        {
            this.ResolveName = new global::System.Func<global::System.Type, string>(this.ResolveNameFromType);
            this.ResolveType = new global::System.Func<string, global::System.Type>(this.ResolveTypeFromName);
            this.OnContextCreated();
        }
        partial void OnContextCreated();
        /// <summary>
        /// Since the namespace configured for this service reference
        /// in Visual Studio is different from the one indicated in the
        /// server schema, use type-mappers to map between the two.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        protected global::System.Type ResolveTypeFromName(string typeName)
        {
            if (typeName.StartsWith("OGDI.delhi", global::System.StringComparison.Ordinal))
            {
                return this.GetType().Assembly.GetType(string.Concat("DpOgdiUI.DelhiPolice", typeName.Substring(10)), false);
            }
            return null;
        }
        /// <summary>
        /// Since the namespace configured for this service reference
        /// in Visual Studio is different from the one indicated in the
        /// server schema, use type-mappers to map between the two.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        protected string ResolveNameFromType(global::System.Type clientType)
        {
            if (clientType.Namespace.Equals("DpOgdiUI.DelhiPolice", global::System.StringComparison.Ordinal))
            {
                return string.Concat("OGDI.delhi.", clientType.Name);
            }
            return null;
        }
        /// <summary>
        /// There are no comments for Crimes in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Data.Services.Client.DataServiceQuery<CrimesItem> Crimes
        {
            get
            {
                if ((this._Crimes == null))
                {
                    this._Crimes = base.CreateQuery<CrimesItem>("Crimes");
                }
                return this._Crimes;
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Data.Services.Client.DataServiceQuery<CrimesItem> _Crimes;
        /// <summary>
        /// There are no comments for Officers in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Data.Services.Client.DataServiceQuery<OfficersItem> Officers
        {
            get
            {
                if ((this._Officers == null))
                {
                    this._Officers = base.CreateQuery<OfficersItem>("Officers");
                }
                return this._Officers;
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Data.Services.Client.DataServiceQuery<OfficersItem> _Officers;
        /// <summary>
        /// There are no comments for PoliceStations in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Data.Services.Client.DataServiceQuery<PoliceStationsItem> PoliceStations
        {
            get
            {
                if ((this._PoliceStations == null))
                {
                    this._PoliceStations = base.CreateQuery<PoliceStationsItem>("PoliceStations");
                }
                return this._PoliceStations;
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Data.Services.Client.DataServiceQuery<PoliceStationsItem> _PoliceStations;
        /// <summary>
        /// There are no comments for Crimes in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public void AddToCrimes(CrimesItem crimesItem)
        {
            base.AddObject("Crimes", crimesItem);
        }
        /// <summary>
        /// There are no comments for Officers in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public void AddToOfficers(OfficersItem officersItem)
        {
            base.AddObject("Officers", officersItem);
        }
        /// <summary>
        /// There are no comments for PoliceStations in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public void AddToPoliceStations(PoliceStationsItem policeStationsItem)
        {
            base.AddObject("PoliceStations", policeStationsItem);
        }
    }
    /// <summary>
    /// There are no comments for OGDI.delhi.CrimesItem in the schema.
    /// </summary>
    /// <KeyProperties>
    /// PartitionKey
    /// RowKey
    /// </KeyProperties>
    [global::System.Data.Services.Common.EntitySetAttribute("Crimes")]
    [global::System.Data.Services.Common.DataServiceKeyAttribute("PartitionKey", "RowKey")]
    public partial class CrimesItem : global::System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Create a new CrimesItem object.
        /// </summary>
        /// <param name="partitionKey">Initial value of PartitionKey.</param>
        /// <param name="rowKey">Initial value of RowKey.</param>
        /// <param name="timestamp">Initial value of Timestamp.</param>
        /// <param name="entityid">Initial value of entityid.</param>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public static CrimesItem CreateCrimesItem(string partitionKey, string rowKey, global::System.DateTime timestamp, global::System.Guid entityid)
        {
            CrimesItem crimesItem = new CrimesItem();
            crimesItem.PartitionKey = partitionKey;
            crimesItem.RowKey = rowKey;
            crimesItem.Timestamp = timestamp;
            crimesItem.entityid = entityid;
            return crimesItem;
        }
        /// <summary>
        /// There are no comments for Property PartitionKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string PartitionKey
        {
            get
            {
                return this._PartitionKey;
            }
            set
            {
                this.OnPartitionKeyChanging(value);
                this._PartitionKey = value;
                this.OnPartitionKeyChanged();
                this.OnPropertyChanged("PartitionKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _PartitionKey;
        partial void OnPartitionKeyChanging(string value);
        partial void OnPartitionKeyChanged();
        /// <summary>
        /// There are no comments for Property RowKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string RowKey
        {
            get
            {
                return this._RowKey;
            }
            set
            {
                this.OnRowKeyChanging(value);
                this._RowKey = value;
                this.OnRowKeyChanged();
                this.OnPropertyChanged("RowKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _RowKey;
        partial void OnRowKeyChanging(string value);
        partial void OnRowKeyChanged();
        /// <summary>
        /// There are no comments for Property Timestamp in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.DateTime Timestamp
        {
            get
            {
                return this._Timestamp;
            }
            set
            {
                this.OnTimestampChanging(value);
                this._Timestamp = value;
                this.OnTimestampChanged();
                this.OnPropertyChanged("Timestamp");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.DateTime _Timestamp;
        partial void OnTimestampChanging(global::System.DateTime value);
        partial void OnTimestampChanged();
        /// <summary>
        /// There are no comments for Property entityid in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Guid entityid
        {
            get
            {
                return this._entityid;
            }
            set
            {
                this.OnentityidChanging(value);
                this._entityid = value;
                this.OnentityidChanged();
                this.OnPropertyChanged("entityid");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Guid _entityid;
        partial void OnentityidChanging(global::System.Guid value);
        partial void OnentityidChanged();
        /// <summary>
        /// There are no comments for Property id in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> id
        {
            get
            {
                return this._id;
            }
            set
            {
                this.OnidChanging(value);
                this._id = value;
                this.OnidChanged();
                this.OnPropertyChanged("id");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _id;
        partial void OnidChanging(global::System.Nullable<int> value);
        partial void OnidChanged();
        /// <summary>
        /// There are no comments for Property ps_id in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> ps_id
        {
            get
            {
                return this._ps_id;
            }
            set
            {
                this.Onps_idChanging(value);
                this._ps_id = value;
                this.Onps_idChanged();
                this.OnPropertyChanged("ps_id");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _ps_id;
        partial void Onps_idChanging(global::System.Nullable<int> value);
        partial void Onps_idChanged();
        /// <summary>
        /// There are no comments for Property incident_count in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> incident_count
        {
            get
            {
                return this._incident_count;
            }
            set
            {
                this.Onincident_countChanging(value);
                this._incident_count = value;
                this.Onincident_countChanged();
                this.OnPropertyChanged("incident_count");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _incident_count;
        partial void Onincident_countChanging(global::System.Nullable<int> value);
        partial void Onincident_countChanged();
        /// <summary>
        /// There are no comments for Property date in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<global::System.DateTime> date
        {
            get
            {
                return this._date;
            }
            set
            {
                this.OndateChanging(value);
                this._date = value;
                this.OndateChanged();
                this.OnPropertyChanged("date");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<global::System.DateTime> _date;
        partial void OndateChanging(global::System.Nullable<global::System.DateTime> value);
        partial void OndateChanged();
        /// <summary>
        /// There are no comments for Property district_id in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> district_id
        {
            get
            {
                return this._district_id;
            }
            set
            {
                this.Ondistrict_idChanging(value);
                this._district_id = value;
                this.Ondistrict_idChanged();
                this.OnPropertyChanged("district_id");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _district_id;
        partial void Ondistrict_idChanging(global::System.Nullable<int> value);
        partial void Ondistrict_idChanged();
        /// <summary>
        /// There are no comments for Property crime_head in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string crime_head
        {
            get
            {
                return this._crime_head;
            }
            set
            {
                this.Oncrime_headChanging(value);
                this._crime_head = value;
                this.Oncrime_headChanged();
                this.OnPropertyChanged("crime_head");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _crime_head;
        partial void Oncrime_headChanging(string value);
        partial void Oncrime_headChanged();
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        protected virtual void OnPropertyChanged(string property)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new global::System.ComponentModel.PropertyChangedEventArgs(property));
            }
        }
    }
    /// <summary>
    /// There are no comments for OGDI.delhi.OfficersItem in the schema.
    /// </summary>
    /// <KeyProperties>
    /// PartitionKey
    /// RowKey
    /// </KeyProperties>
    [global::System.Data.Services.Common.EntitySetAttribute("Officers")]
    [global::System.Data.Services.Common.DataServiceKeyAttribute("PartitionKey", "RowKey")]
    public partial class OfficersItem : global::System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Create a new OfficersItem object.
        /// </summary>
        /// <param name="partitionKey">Initial value of PartitionKey.</param>
        /// <param name="rowKey">Initial value of RowKey.</param>
        /// <param name="timestamp">Initial value of Timestamp.</param>
        /// <param name="entityid">Initial value of entityid.</param>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public static OfficersItem CreateOfficersItem(string partitionKey, string rowKey, global::System.DateTime timestamp, global::System.Guid entityid)
        {
            OfficersItem officersItem = new OfficersItem();
            officersItem.PartitionKey = partitionKey;
            officersItem.RowKey = rowKey;
            officersItem.Timestamp = timestamp;
            officersItem.entityid = entityid;
            return officersItem;
        }
        /// <summary>
        /// There are no comments for Property PartitionKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string PartitionKey
        {
            get
            {
                return this._PartitionKey;
            }
            set
            {
                this.OnPartitionKeyChanging(value);
                this._PartitionKey = value;
                this.OnPartitionKeyChanged();
                this.OnPropertyChanged("PartitionKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _PartitionKey;
        partial void OnPartitionKeyChanging(string value);
        partial void OnPartitionKeyChanged();
        /// <summary>
        /// There are no comments for Property RowKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string RowKey
        {
            get
            {
                return this._RowKey;
            }
            set
            {
                this.OnRowKeyChanging(value);
                this._RowKey = value;
                this.OnRowKeyChanged();
                this.OnPropertyChanged("RowKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _RowKey;
        partial void OnRowKeyChanging(string value);
        partial void OnRowKeyChanged();
        /// <summary>
        /// There are no comments for Property Timestamp in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.DateTime Timestamp
        {
            get
            {
                return this._Timestamp;
            }
            set
            {
                this.OnTimestampChanging(value);
                this._Timestamp = value;
                this.OnTimestampChanged();
                this.OnPropertyChanged("Timestamp");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.DateTime _Timestamp;
        partial void OnTimestampChanging(global::System.DateTime value);
        partial void OnTimestampChanged();
        /// <summary>
        /// There are no comments for Property entityid in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Guid entityid
        {
            get
            {
                return this._entityid;
            }
            set
            {
                this.OnentityidChanging(value);
                this._entityid = value;
                this.OnentityidChanged();
                this.OnPropertyChanged("entityid");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Guid _entityid;
        partial void OnentityidChanging(global::System.Guid value);
        partial void OnentityidChanged();
        /// <summary>
        /// There are no comments for Property id in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string id
        {
            get
            {
                return this._id;
            }
            set
            {
                this.OnidChanging(value);
                this._id = value;
                this.OnidChanged();
                this.OnPropertyChanged("id");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _id;
        partial void OnidChanging(string value);
        partial void OnidChanged();
        /// <summary>
        /// There are no comments for Property name in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this.OnnameChanging(value);
                this._name = value;
                this.OnnameChanged();
                this.OnPropertyChanged("name");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _name;
        partial void OnnameChanging(string value);
        partial void OnnameChanged();
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        protected virtual void OnPropertyChanged(string property)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new global::System.ComponentModel.PropertyChangedEventArgs(property));
            }
        }
    }
    /// <summary>
    /// There are no comments for OGDI.delhi.PoliceStationsItem in the schema.
    /// </summary>
    /// <KeyProperties>
    /// PartitionKey
    /// RowKey
    /// </KeyProperties>
    [global::System.Data.Services.Common.EntitySetAttribute("PoliceStations")]
    [global::System.Data.Services.Common.DataServiceKeyAttribute("PartitionKey", "RowKey")]
    public partial class PoliceStationsItem : global::System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Create a new PoliceStationsItem object.
        /// </summary>
        /// <param name="partitionKey">Initial value of PartitionKey.</param>
        /// <param name="rowKey">Initial value of RowKey.</param>
        /// <param name="timestamp">Initial value of Timestamp.</param>
        /// <param name="entityid">Initial value of entityid.</param>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public static PoliceStationsItem CreatePoliceStationsItem(string partitionKey, string rowKey, global::System.DateTime timestamp, global::System.Guid entityid)
        {
            PoliceStationsItem policeStationsItem = new PoliceStationsItem();
            policeStationsItem.PartitionKey = partitionKey;
            policeStationsItem.RowKey = rowKey;
            policeStationsItem.Timestamp = timestamp;
            policeStationsItem.entityid = entityid;
            return policeStationsItem;
        }
        /// <summary>
        /// There are no comments for Property PartitionKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string PartitionKey
        {
            get
            {
                return this._PartitionKey;
            }
            set
            {
                this.OnPartitionKeyChanging(value);
                this._PartitionKey = value;
                this.OnPartitionKeyChanged();
                this.OnPropertyChanged("PartitionKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _PartitionKey;
        partial void OnPartitionKeyChanging(string value);
        partial void OnPartitionKeyChanged();
        /// <summary>
        /// There are no comments for Property RowKey in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string RowKey
        {
            get
            {
                return this._RowKey;
            }
            set
            {
                this.OnRowKeyChanging(value);
                this._RowKey = value;
                this.OnRowKeyChanged();
                this.OnPropertyChanged("RowKey");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _RowKey;
        partial void OnRowKeyChanging(string value);
        partial void OnRowKeyChanged();
        /// <summary>
        /// There are no comments for Property Timestamp in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.DateTime Timestamp
        {
            get
            {
                return this._Timestamp;
            }
            set
            {
                this.OnTimestampChanging(value);
                this._Timestamp = value;
                this.OnTimestampChanged();
                this.OnPropertyChanged("Timestamp");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.DateTime _Timestamp;
        partial void OnTimestampChanging(global::System.DateTime value);
        partial void OnTimestampChanged();
        /// <summary>
        /// There are no comments for Property entityid in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Guid entityid
        {
            get
            {
                return this._entityid;
            }
            set
            {
                this.OnentityidChanging(value);
                this._entityid = value;
                this.OnentityidChanged();
                this.OnPropertyChanged("entityid");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Guid _entityid;
        partial void OnentityidChanging(global::System.Guid value);
        partial void OnentityidChanged();
        /// <summary>
        /// There are no comments for Property id in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> id
        {
            get
            {
                return this._id;
            }
            set
            {
                this.OnidChanging(value);
                this._id = value;
                this.OnidChanged();
                this.OnPropertyChanged("id");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _id;
        partial void OnidChanging(global::System.Nullable<int> value);
        partial void OnidChanged();
        /// <summary>
        /// There are no comments for Property stationname in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string stationname
        {
            get
            {
                return this._stationname;
            }
            set
            {
                this.OnstationnameChanging(value);
                this._stationname = value;
                this.OnstationnameChanged();
                this.OnPropertyChanged("stationname");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _stationname;
        partial void OnstationnameChanging(string value);
        partial void OnstationnameChanged();
        /// <summary>
        /// There are no comments for Property districtid in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<int> districtid
        {
            get
            {
                return this._districtid;
            }
            set
            {
                this.OndistrictidChanging(value);
                this._districtid = value;
                this.OndistrictidChanged();
                this.OnPropertyChanged("districtid");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<int> _districtid;
        partial void OndistrictidChanging(global::System.Nullable<int> value);
        partial void OndistrictidChanged();
        /// <summary>
        /// There are no comments for Property shoname in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string shoname
        {
            get
            {
                return this._shoname;
            }
            set
            {
                this.OnshonameChanging(value);
                this._shoname = value;
                this.OnshonameChanged();
                this.OnPropertyChanged("shoname");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _shoname;
        partial void OnshonameChanging(string value);
        partial void OnshonameChanged();
        /// <summary>
        /// There are no comments for Property shoid in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string shoid
        {
            get
            {
                return this._shoid;
            }
            set
            {
                this.OnshoidChanging(value);
                this._shoid = value;
                this.OnshoidChanged();
                this.OnPropertyChanged("shoid");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _shoid;
        partial void OnshoidChanging(string value);
        partial void OnshoidChanged();
        /// <summary>
        /// There are no comments for Property shophone in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string shophone
        {
            get
            {
                return this._shophone;
            }
            set
            {
                this.OnshophoneChanging(value);
                this._shophone = value;
                this.OnshophoneChanged();
                this.OnPropertyChanged("shophone");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _shophone;
        partial void OnshophoneChanging(string value);
        partial void OnshophoneChanged();
        /// <summary>
        /// There are no comments for Property shomobile in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string shomobile
        {
            get
            {
                return this._shomobile;
            }
            set
            {
                this.OnshomobileChanging(value);
                this._shomobile = value;
                this.OnshomobileChanged();
                this.OnPropertyChanged("shomobile");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _shomobile;
        partial void OnshomobileChanging(string value);
        partial void OnshomobileChanged();
        /// <summary>
        /// There are no comments for Property shoemail in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string shoemail
        {
            get
            {
                return this._shoemail;
            }
            set
            {
                this.OnshoemailChanging(value);
                this._shoemail = value;
                this.OnshoemailChanged();
                this.OnPropertyChanged("shoemail");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _shoemail;
        partial void OnshoemailChanging(string value);
        partial void OnshoemailChanged();
        /// <summary>
        /// There are no comments for Property address in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string address
        {
            get
            {
                return this._address;
            }
            set
            {
                this.OnaddressChanging(value);
                this._address = value;
                this.OnaddressChanged();
                this.OnPropertyChanged("address");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _address;
        partial void OnaddressChanging(string value);
        partial void OnaddressChanged();
        /// <summary>
        /// There are no comments for Property latitude in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<double> latitude
        {
            get
            {
                return this._latitude;
            }
            set
            {
                this.OnlatitudeChanging(value);
                this._latitude = value;
                this.OnlatitudeChanged();
                this.OnPropertyChanged("latitude");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<double> _latitude;
        partial void OnlatitudeChanging(global::System.Nullable<double> value);
        partial void OnlatitudeChanged();
        /// <summary>
        /// There are no comments for Property longitude in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public global::System.Nullable<double> longitude
        {
            get
            {
                return this._longitude;
            }
            set
            {
                this.OnlongitudeChanging(value);
                this._longitude = value;
                this.OnlongitudeChanged();
                this.OnPropertyChanged("longitude");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private global::System.Nullable<double> _longitude;
        partial void OnlongitudeChanging(global::System.Nullable<double> value);
        partial void OnlongitudeChanged();
        /// <summary>
        /// There are no comments for Property kmlsnippet in the schema.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public string kmlsnippet
        {
            get
            {
                return this._kmlsnippet;
            }
            set
            {
                this.OnkmlsnippetChanging(value);
                this._kmlsnippet = value;
                this.OnkmlsnippetChanged();
                this.OnPropertyChanged("kmlsnippet");
            }
        }
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        private string _kmlsnippet;
        partial void OnkmlsnippetChanging(string value);
        partial void OnkmlsnippetChanged();
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Services.Design", "1.0.0")]
        protected virtual void OnPropertyChanged(string property)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new global::System.ComponentModel.PropertyChangedEventArgs(property));
            }
        }
    }
}
