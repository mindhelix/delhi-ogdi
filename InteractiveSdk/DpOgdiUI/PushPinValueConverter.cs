﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace DpOgdiUI
{
    /// <summary>
    /// Convert a ZoomLevel into to ScaleTransform
    /// </summary>
    public class PushPinScaleConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Convert a ZoomLevel into to Scale
        /// </summary>
        //
        // The published Bing Maps Zoom scaling factors, in meters/pixel
        //     http://msdn.microsoft.com/en-us/library/aa940990.aspx
        //
        // Bing maps resolution in meters/pixel is defined are by the equation:
        //     (78271.52 / (2^(ZoomLevel-1))) * Cos(Latitude)
        //

        static private double[] BingMapsScaleFactor = { 78271.52, 78271.52, 39135.76, 19567.88,
                                                         9783.94,  4891.97,  2445.98,  1222.99,
                                                          611.50,   305.75,   152.87,    76.44,
                                                           38.22,    19.11,     9.55,     4.78,
                                                            2.39,     1.19,     0.60,     0.30 };

        /// <summary>
        /// Convert a Bing Maps ZoomLevel into meters per pixel, assumes at the equator.
        /// </summary>
        /// <param name="zoomLevel">The current ZoomLevel</param>
        /// <returns>The scale in meters per pixel</returns>
        static public double BingMapsScaleMetersPerPixel(double zoomLevel)
        {
            if (zoomLevel < 0 || zoomLevel > (BingMapsScaleFactor.Length - 1))
                return BingMapsScaleFactor[0];

            return BingMapsScaleFactor[(int)Math.Round(zoomLevel)];
        }

        /// <summary>
        /// Convert a Bing Maps ZoomLevel into meters per pixel, scaled with latitude
        /// </summary>
        /// <param name="zoomLevel">The current ZoomLevel</param>
        /// <param name="latitude">The current Latitude</param>
        /// <returns>The scale in meters per pixel</returns>
        static public double BingMapsScaleMetersPerPixel(double zoomLevel, double latitude)
        {
            if (zoomLevel < 0 || zoomLevel > BingMapsScaleFactor.Length)
                return BingMapsScaleFactor[0];

            return BingMapsScaleFactor[(int)Math.Round(zoomLevel)] * Math.Cos(Math.PI * latitude / 180);
        }

        /// <summary>
        /// Convert a ZoomLevel into to ScaleTransform
        /// </summary>
        /// <param name="value">The current ZoomLevel</param>
        /// <param name="targetType">The type of the target (unused)</param>
        /// <param name="parameter">The parameter (unused)</param>
        /// <param name="culture">The current culture (unused)</param>
        /// <returns>A ScaleTransform</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double currentZoomLevel = (double)value;

            //
            // A PushPin is normally 35x35 pixels, at zoom level 15 we wouldlike that to be 1:1
            //

            var ScaleVal = BingMapsScaleFactor[13] / BingMapsScaleMetersPerPixel(currentZoomLevel);

            var transform = new ScaleTransform();
            transform.ScaleX = ScaleVal;
            transform.ScaleY = ScaleVal;

            //
            // Set the transform center X and Y so the Pushpin stays at the correct location.
            // The Default Pushpin's height is 35 and width is 34
            // Since the Default Pushpin's PositionOrigin is set to BottomCenter, we need to
            // set the CenterX to half the width (17), and CenterY to the height (35).
            //

            transform.CenterX = 17;
            transform.CenterY = 35;

            return transform;
        }

        /// <summary>
        /// Convert a ScaleTransform into to ZoomeLevel
        /// </summary>
        /// <param name="value">The current ScaleTransform</param>
        /// <param name="targetType">The type of the target (unused)</param>
        /// <param name="parameter">The parameter (unused)</param>
        /// <param name="culture">The current culture (unused)</param>
        /// <returns>A ZoomeLevel</returns>
        /// <exception cref="NotImplementedException">Always thrown</exception>
        /// <remarks>Unimplemented</remarks>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
