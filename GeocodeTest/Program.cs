﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeocodeTest.GoogleMaps;
using GeocodeTest.CitizenReports;
//using GeocodeTest.GoogleMaps.Google.Api.Maps.Service.Geocoding;

namespace GeocodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var port = 81;
            Console.WriteLine("Enter port : ");
            port = Int32.Parse(Console.ReadLine());
            var url = "http://localhost:" + port + "/CitizenFeedback/CitizenReports.svc";
            var client = new CitizenReportsClient("*",url);

//            testGeo(port);
//            return;
            var reportObj = new ReportObject();
            reportObj.date_time = DateTime.UtcNow;

            reportObj.category = "Gambling Activity";
            reportObj.latitude = 28.4567;
            reportObj.longitude = 70.4342;
            reportObj.name = "John Smith";
            reportObj.details = "Testing 1..2...3... count";
            reportObj.email = "test@test.com";
            reportObj.ps_id = 101;
            reportObj.policestation = "Kotwali";
            client.postCitizenReport(reportObj);
        }
        public static void testGeo(int port)
        {
            var url = "http://localhost:" + port + "/Geocode/GoogleMaps.svc";
            var client = new GoogleMaps.GoogleMapsClient();
            var req = new GeocodingRequest();
            req.LatitudeLongitude = "28.6434,77.3434";
            
            var resp = client.DoRequest(req);
            Console.Write("oo");
        }
    }
}
