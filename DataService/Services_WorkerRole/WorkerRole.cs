using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;
using System.IO;
using System.Xml;
using InterIMAP;
using System.Text.RegularExpressions;
using System.Web;
using ServiceData;
using Services_WorkerRole.MessageHandlers;
using TweetSharp;
using System.Text;
using ServiceData.Utilities;

namespace Services_WorkerRole
{

    public class WorkerRole : RoleEntryPoint
    {
        private CloudQueue queue;
        private CloudBlobContainer container;
        private CloudTableClient tableClient;
        private IDictionary<string, IMessageHandler> handlers;

        public override void Run()
        {
            InitializeHandlers();

            // This is a sample worker implementation. Replace with your logic.
            Trace.WriteLine("$projectname$ entry point called", "Information");

            while (true)
            {
                CloudQueueMessage msg = queue.GetMessage();

                if (msg != null)
                {
                    queue.DeleteMessage(msg);
                    ProcessMessage(msg);
                }
                // Send Weekly DCP email
                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                {

                }
                // Connect to IMAP and check for mail
                //var replies = CheckEmail();
                //ProcessEmails(replies);

                // Search Twitter for hashtag and add feedbacks
                CheckTwitter();
                Thread.Sleep(300000);

            }
        }

        private void InitializeHandlers()
        {
            handlers = new Dictionary<string, IMessageHandler>
			{				
                {"SendMail", new SendMailHandler()},
                {"ConvertData", new ConvertDataHandler(container)}                
			};
        }
        public void ProcessMessage(CloudQueueMessage msg)
        {
            var msgStr = msg.AsString;
            using (var strReader = new StringReader(msgStr ?? string.Empty))
            using (var xmlReader = XmlReader.Create(strReader))
            {
                xmlReader.ReadToDescendant("command");
                var commandName = xmlReader.GetAttribute("commandname") ?? string.Empty;
                IMessageHandler handler;
                if (handlers.TryGetValue(commandName, out handler))
                {
                    var body = xmlReader.ReadInnerXml();
                    handler.ProcessMessage(body);
                }
            }
        }
        private void CheckTwitter()
        {
            var tweets = SearchTwitter(ServiceConfig.TWITTER_HASHTAG);
            ProcessTweets(tweets);
        }
        private void ProcessTweets(List<TwitterSearchStatus> tweets)
        {
            foreach (var tweet in tweets)
            {
                if (tweet.GeoLocation != null)
                {
                    var lat = tweet.GeoLocation.Coordinates.Latitude;
                    var lon = tweet.GeoLocation.Coordinates.Longitude;

                    // lat = 28.635308;
                    // lon = 77.224445;

                    var feedback = tweet.Text;
                    var id = "Tweet_" + tweet.Id;

                    var tableContext = tableClient.GetDataServiceContext();
                    var tableName = "CitizenReport";

                    var reps = (from r in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                where r.RowKey == id
                                select r).FirstOrDefault();
                    if (reps != null) continue; // Tweet already in db

                    var stn = PolygonUtils.FindStationContainingLocation(polygons,
                        new Location { Latitude = lat, Longitude = lon, Altitude = 0.0 }
                    );

                    if (stn == null)
                    {
                        continue;
                    }

                    //var psid = PolygonUtils.
                    var report = new ReportObject
                    {
                        latitude = lat,
                        longitude = lon,
                        details = feedback,
                        ps_id = stn.id,
                        policestation = stn.stationname
                    };
                    var reportEnt = new ReportObjectEntity(report);
                    reportEnt.RowKey = id;
                    tableContext.AddObject(tableName, reportEnt);
                    tableContext.SaveChangesWithRetries();
                }
                else
                {
                    var tableContext = tableClient.GetDataServiceContext();
                    tableClient.CreateTableIfNotExist(ServiceConfig.TABLE_TWEETSREAD);
                    var tweetread = (from r in tableContext.CreateQuery<ReadTweetEntity>(ServiceConfig.TABLE_TWEETSREAD)
                                     where r.RowKey == "Tweet_" + tweet.Id
                                     select r).FirstOrDefault();
                    if (tweetread == null)
                    {
                        SendDirectMessage(tweet.Author.ScreenName, "Please use a geo-tweeting client like Seesmic ( http://www.seesmic.com ) so that we can pick up the location of your feedback");

                        var twtObj = new ReadTweetEntity(tweet);
                        tableContext.AddObject(ServiceConfig.TABLE_TWEETSREAD, twtObj);
                        tableContext.SaveChangesWithRetries();
                    }
                }
            }
        }
        private void SendDirectMessage(string recipient, string message)
        {
            TwitterService service = new TwitterService(ServiceConfig.TWITTER_CONSUMER_KEY, ServiceConfig.TWITTER_CONSUMER_SECRET, ServiceConfig.TWITTER_ACCESS_TOKEN, ServiceConfig.TWITTER_ACCESS_SECRET);
            service.SendDirectMessage(recipient, message);
        }
        private List<TwitterSearchStatus> SearchTwitter(string hashtag)
        {
            TwitterService s = new TwitterService();
            List<TwitterSearchStatus> output = new List<TwitterSearchStatus>();
            for (int i = 1; ; i++)
            {
                StringBuilder sb = new StringBuilder();

                var result = s.Search(hashtag, i, 20);
                if (result == null) break;
                int x = result.Statuses.Count();
                
                if (x > 0)
                {
                    output.AddRange(result.Statuses);
                }
                else
                {
                    break;
                }
            }
            return output;
        }
        private void ProcessEmails(List<EmailMessage> messages)
        {
            if (messages.Count > 0)
            {
                foreach (var reply in messages)
                {
                    TableServiceContext tableContext = tableClient.GetDataServiceContext();

                    var report = (from r in
                                      tableContext.CreateQuery<ReportObjectEntity>(ServiceConfig.TABLE_CITIZENREPORTS)
                                  where r.RowKey == reply.FeedbackNumber
                                  select r).FirstOrDefault();
                    if (report != null)
                    {
                        //var query = from e in context
                        if (reply.FromAddress.Contains("sho"))
                        {
                            report.sho_reply = reply.FeedbackReply;
                            report.sho_reply_time = reply.ReplyTime;

                            report.dcp_reply_time = (!report.dcp_reply_time.Equals(DateTime.MinValue)) ? report.dcp_reply_time : DateTime.UtcNow;
                            report.read_time = (!report.read_time.Equals(DateTime.MinValue)) ? report.read_time : DateTime.UtcNow;
                            report.sho_read_time = (!report.sho_read_time.Equals(DateTime.MinValue)) ? report.sho_read_time : DateTime.UtcNow;
                        }
                        else if (reply.FromAddress.Contains("dcp"))
                        {
                            report.dcp_reply = reply.FeedbackReply;
                            report.dcp_reply_time = reply.ReplyTime;

                            report.read_time = (!report.read_time.Equals(DateTime.MinValue)) ? report.read_time : DateTime.UtcNow;
                            report.sho_read_time = (!report.sho_read_time.Equals(DateTime.MinValue)) ? report.sho_read_time : DateTime.UtcNow;
                        }
                        tableContext.UpdateObject(report);
                        try
                        {
                            tableContext.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            ;
                        }
                    }
                }
            }
        }
        private List<EmailMessage> CheckEmail()
        {
            IMAPConfig config = new IMAPConfig(ServiceConfig.IMAP_SERVER_ADDRESS, ServiceConfig.EMAIL_SERVER_LOGIN, ServiceConfig.EMAIL_SERVER_PASSWORD, true, true, "Inbox");
            IMAPClient client = null;
            try
            {
                client = new IMAPClient(config, null, 5);
            }
            catch (IMAPException e)
            {
                Console.WriteLine(e.Message);
                // return;
            }

            IMAPFolder f = new IMAPFolder();
            f = client.Folders["INBOX"];
            var q = IMAPSearchQuery.QuickSearchUnread();
            IMAPSearchResult sResult = f.Search(q);
            List<EmailMessage> fields = new List<EmailMessage>();
            foreach (var m in sResult.Messages)
            {
                if (m.BodyParts.Count > 0)
                {
                    EmailMessage em = new EmailMessage();
                    em.FromAddress = m.From[0].Address;
                    em.Subject = m.Subject;

                    Regex regex = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>", RegexOptions.Singleline);
                    if(m.HtmlData!=null)
                        em.FeedbackReply = DecodeQuotedPrintables(m.HtmlData.TextData);
                    else
                        em.FeedbackReply = DecodeQuotedPrintables(m.BodyParts[0].TextData);

                    em.FeedbackReply = Regex.Replace(em.FeedbackReply, @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>", String.Empty);

                    if (em.FeedbackReply.IndexOf("----- Original Message -----") > 0)
                    {
                        em.FeedbackReply = em.FeedbackReply.Substring(0, em.FeedbackReply.IndexOf("----- Original Message -----"));
                    }
                    em.FeedbackReply = em.FeedbackReply.Replace("\\n", "\n");
                    em.FeedbackReply = em.FeedbackReply.Replace("\\r", "\r");
                    em.ReplyTime = m.Date;
                    Match match = Regex.Match(em.Subject, @"Feedback Number \(([^)]+)\)", RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        em.FeedbackNumber = match.Groups[1].Value;
                        fields.Add(em);
                        sResult.Folder.Select();
                        m.MarkAsRead();
                        sResult.Folder.Examine();
                    }
                }

            }
            client.Logoff();
            return fields;
        }
        private string DecodeQuotedPrintables(string input)
        {
            var occurences = new Regex(@"=[0-9A-Z]{2}", RegexOptions.Multiline);
            var matches = occurences.Matches(input);
            foreach (Match match in matches)
            {
                char hexChar = (char)Convert.ToInt32(match.Groups[0].Value.Substring(1), 16);
                input = input.Replace(match.Groups[0].Value, hexChar.ToString());
            }
            return input.Replace("=\r\n", "");
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            //DiagnosticMonitor.Start("DiagnosticsConnectionString");

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            RoleEnvironment.Changing += RoleEnvironmentChanging;

            // read storage account configuration settings
            CloudStorageAccount.SetConfigurationSettingPublisher((configName, configSetter)
                => configSetter(RoleEnvironment.GetConfigurationSettingValue(configName)));

            var storageAccount = CloudStorageAccount.FromConfigurationSetting("OgdiConfigConnectionString");
            // initialize blob storage
            CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
            container = blobStorage.GetContainerReference("converteddata");

            // initialize queue storage 
            CloudQueueClient queueStorage = storageAccount.CreateCloudQueueClient();
            queue = queueStorage.GetQueueReference("workercommands");

            tableClient = StorageSettings.GetInstance().getCloudTableClient();

            Trace.TraceInformation("Creating container and queue...");

            bool storageInitialized = false;
            while (!storageInitialized)
            {
                try
                {
                    // create the blob container and allow public access
                    container.CreateIfNotExist();
                    var permissions = container.GetPermissions();
                    permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                    container.SetPermissions(permissions);

                    // create the message queue
                    queue.CreateIfNotExist();
                    storageInitialized = true;
                }
                catch (StorageClientException e)
                {
                    if (e.ErrorCode == StorageErrorCode.TransportError)
                    {
                        Trace.TraceError("Storage services initialization failure. "
                          + "Check your storage account configuration settings. If running locally, "
                          + "ensure that the Development Storage service is running. Message: '{0}'", e.Message);
                        Thread.Sleep(5000);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            var client = StorageSettings.GetInstance().getCloudDataTableClient();
            var tableName = ServiceConfig.TABLE_POLICESTATIONS;
            var context = client.GetDataServiceContext();
            stations = (from g in context.CreateQuery<PoliceStation>(tableName)
                            select g).ToList<PoliceStation>();
            polygons = PolygonUtils.ParseStations(stations);
            return base.OnStart();
        }
        private List<PoliceStation> stations = null;
        private List<Polygon> polygons = null;

        private static void RoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e)
        {
            // If a configuration setting is changing
            if (e.Changes.Any(change => change is RoleEnvironmentConfigurationSettingChange))
            {
                // Set e.Cancel to true to restart this role instance
                e.Cancel = true;
            }
        }
    }
    public class EmailMessage
    {
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public DateTime ReplyTime { get; set; }
        public string FeedbackNumber { get; set; }
        public string FeedbackReply { get; set; }
    }
}
