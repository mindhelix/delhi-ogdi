﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using ServiceData;

namespace ServiceData.Utilities
{
    public class PolygonUtils
    {
        public static SimplePSEntity FindStationContainingLocation(List<Polygon> polygons, Location loc)
        {
            var poly = FindPolygonContainingPoints(polygons, loc.Latitude, loc.Longitude);
            if (poly != null)
            {
                return poly.Station;
            }
            else
            {
                return null;
            }
        }
        public static Polygon FindPolygonContainingPoints(List<Polygon> polygons,double lat, double lon)
        {
            foreach (var p in polygons)
            {
                if (p.pointInPolygon(lat, lon))
                {
                    return p;
                }
            }
            return null;
        }
        public static List<Polygon> ParseKml(string kml)
        {
            XDocument doc = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+kml);
            XNamespace ns = XNamespace.Get("http://www.opengis.net/kml/2.2");
                
            var placemarks = from p in doc.Descendants(ns+"Placemark")
                                select p;
            
            var polys = new List<Polygon>();

            var cnt = 0;

            foreach (var placemark in placemarks)
            {
                var p = placemark;
                var Name = p.Element(ns+"name").Value;
                var Coords = p.Descendants(ns + "coordinates").FirstOrDefault();
                if (Coords != null)
                {
                    var polyStr = Coords.Value;
                    var polygon = Polygon.Parse(polyStr);
                    if (polygon.Count != 0)
                    {
                        polygon.Name = Name;
                        polygon.Id = 0;
                        polys.Add(polygon);
                    }
                }
            }
            return polys;
        }
        public static List<Polygon> ParseStations(List<PoliceStation> stations)
        {
            var polys = new List<Polygon>();
            foreach (var stn in stations)
            {
                if (stn.kmlsnippet == null) continue;
                XDocument p = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + stn.kmlsnippet);
                var Name = stn.stationname;

                //var Name = p.Element("name").Value;
                var Coords = p.Descendants("coordinates").FirstOrDefault();
                if (Coords != null)
                {
                    var polyStr = Coords.Value;
                    var polygon = Polygon.Parse(polyStr);
                    if (polygon.Count != 0)
                    {
                        polygon.Name = Name;
                        polygon.Id = 0;
                        polygon.Station = new SimplePSEntity { id = stn.id, stationname = stn.stationname };
                        polys.Add(polygon);
                    }
                }
            } 
            return polys;
        }
    }
}