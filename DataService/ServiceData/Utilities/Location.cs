﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceData.Utilities
{
    public class Location
    {
        public Location() { }
        public Location(double Latitude, double Longitude, double Altitude = 0.0)
        {
            this.Latitude = Latitude;
            this.Longitude = Longitude;
            this.Altitude = Altitude;
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
    }
}