﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;
using ServiceData;

namespace ServiceData.Utilities
{
    public class Polygon : List<Location>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SimplePSEntity Station { get; set; }
        public bool pointInPolygon(double lat, double lon)
        {
            int i;
            int j = this.Count - 1;
            bool inPoly = false;

            for (i = 0; i < this.Count; i++)
            {
                if (this[i].Longitude < lon && this[j].Longitude >= lon
                    || this[j].Longitude < lon && this[i].Longitude >= lon)
                {
                    if (this[i].Latitude + (lon - this[i].Longitude) /
                        (this[j].Longitude - this[i].Longitude) * (this[j].Latitude
                        - this[i].Latitude) < lat)
                    {
                        inPoly = !inPoly;
                    }
                }
                j = i;
            }
            return inPoly;
        }
        private static readonly Regex Expression = CreateRegex();
        private static Regex CreateRegex()
        {
            // 16.9.1 coordinatesType description
            // String representing one or more coordinate tuples, with each tuple
            // consisting of decimal values for geodetic longitude, geodetic
            // latitude, and altitude. The altitude component is optional. The
            // coordinate separator is a comma and the tuple separator is a
            // whitespace. Longitude and latitude coordinates are expressed in
            // decimal degrees only.
            const string Seperator = @"\s*,\s*";
            const string Token = @"[^\s,]+";
            const string Longitude = "(?<lon>" + Token + ")" + Seperator;
            const string Latitude = "(?<lat>" + Token + ")";
            const string Altitude = "(?:" + Seperator + "(?<alt>" + Token + "))?"; // Optional
            const string Expression = Longitude + Latitude + Altitude + "\\b";

            return new Regex(Expression, RegexOptions.CultureInvariant);
        }
        public static Polygon Parse(string input)
        {
            var polygon = new Polygon();
            polygon.Clear();
            foreach (Match match in Expression.Matches(input))
            {
                // Minimum required fields for a valid coordinate are latitude and longitude.
                double latitude, longitude;
                if (double.TryParse(match.Groups["lat"].Value, NumberStyles.Float, CultureInfo.InvariantCulture, out latitude) &&
                    double.TryParse(match.Groups["lon"].Value, NumberStyles.Float, CultureInfo.InvariantCulture, out longitude))
                {
                    Group altitudeGroup = match.Groups["alt"];
                    if (altitudeGroup.Success)
                    {
                        double altitude;
                        if (double.TryParse(altitudeGroup.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out altitude))
                        {
                            polygon.Add(new Location(latitude, longitude, altitude));
                            continue; // Success!
                        }
                    }
                    polygon.Add(new Location(latitude, longitude));
                }
            }
            return polygon;
        }
    }
}