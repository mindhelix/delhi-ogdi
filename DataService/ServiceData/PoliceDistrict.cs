﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.StorageClient;

namespace ServiceData
{
    public class PoliceDistrictEntity : TableServiceEntity
    {
        public string dcpemail { get; set; }
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public int id { get; set; }
        public Guid entityid { get; set; }
        public string name { get; set; }
        public string districtrange { get; set; }
    }
}