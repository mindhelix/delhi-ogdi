﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.WindowsAzure.StorageClient;
using System.ServiceModel.Channels;
using System.ServiceModel;
using TweetSharp;

namespace ServiceData
{

    //------- Citizen Report DataContract Class

    [DataContract]
    public class ReportObject
    {
        public ReportObject()
        {
            //default values
            approval_stat = false;
            sho_reply = "";
            dcp_reply = "";
            dcp_reply_public = false;
            sho_read_receipt = false;
            dcp_reply_time = DateTime.UtcNow;
            read_time = DateTime.UtcNow;
            sho_read_time = DateTime.UtcNow;
            sho_reply_time = DateTime.UtcNow;
            category = "";
        }

        [DataMember]
        public int ps_id { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string phone { get; set; }

        [DataMember]
        public string email { get; set; }

        [DataMember]
        public string details { get; set; }

        [DataMember]
        public DateTime date_time { get; set; }

        [DataMember]
        public double latitude { get; set; }

        [DataMember]
        public double longitude { get; set; }

        [DataMember]
        public string location_name { get; set; }

        [DataMember]
        public bool approval_stat { get; set; }

        [DataMember]
        public string policestation { get; set; }

        [DataMember]
        public string ipaddress { get; set; }

        [DataMember]
        public string imageid { get; set; }

        // new properties
        [DataMember]
        public DateTime read_time { get; set; }

        [DataMember]
        public string sho_reply;

        [DataMember]
        public DateTime sho_reply_time;

        [DataMember]
        public string dcp_reply;

        [DataMember]
        public bool dcp_reply_public;

        [DataMember]
        public DateTime dcp_reply_time;

        [DataMember]
        public DateTime sho_read_time;

        [DataMember]
        public bool sho_read_receipt;

        [DataMember]
        public string category { get; set; }
    }
    public class SimplePSEntity
    {
        public int id { get; set; }
        public string stationname { get; set; }
    }

    public class ReadTweetEntity : TableServiceEntity
    {
        public string tweet { get; set; }
        public ReadTweetEntity()
        {
            this.Timestamp = DateTime.UtcNow;
        }
        public ReadTweetEntity(TwitterSearchStatus tweet)
        {
            this.PartitionKey = tweet.Author.ScreenName;
            this.RowKey = "Tweet_" + tweet.Id;
            this.tweet = tweet.Text;
            this.Timestamp = DateTime.UtcNow;
        }
        public ReadTweetEntity(string tweetid, string recipient,string tweet)
        {
            this.PartitionKey = recipient;
            this.RowKey = "Tweet_"+tweetid;
            this.tweet = tweet;
            this.Timestamp = DateTime.UtcNow;
        }
    }
    //ReportObject Entity class for Azure Table Storage
    public class ReportObjectEntity : TableServiceEntity
    {
        public ReportObjectEntity(ReportObject reportObj)
        {
            this.PartitionKey = reportObj.ps_id.ToString();
            this.RowKey = StorageSettings.GetInstance().GetIdGenerator().NextId("citizenFeedbacks").ToString();//DateTime.UtcNow.Ticks.ToString()+"_"+Guid.NewGuid().ToString();

            if (reportObj.ps_id > 0)
            {
                var context = StorageSettings.GetInstance().getCloudDataTableClient().GetDataServiceContext();
                var ps = (from p in context.CreateQuery<PoliceStation>(ServiceConfig.TABLE_POLICESTATIONS)
                         where p.RowKey == this.PartitionKey
                         select p).FirstOrDefault();
                if (ps != null)
                {
                    this.sms_sho_phone = ps.shomobile;
                }
            }
            this.sms_sent = false;

            this.date_time = DateTime.UtcNow;// reportObj.date_time;
            this.latitude = reportObj.latitude;
            this.longitude = reportObj.longitude;
            this.location_name = reportObj.location_name;
            this.phone = reportObj.phone;
            this.email = reportObj.email;
            this.name = reportObj.name;
            this.details = reportObj.details;

            if (OperationContext.Current != null)
            {
                MessageProperties messageProperties = OperationContext.Current.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty =
                    messageProperties[RemoteEndpointMessageProperty.Name]
                    as RemoteEndpointMessageProperty;

                this.ipaddress = endpointProperty.Address;
            }
            this.policestation = reportObj.policestation;
            this.approval_stat = false;


            //new properties
            this.sho_reply = reportObj.sho_reply;
            this.sho_reply_time = DateTime.UtcNow;// reportObj.sho_reply_time;
            this.dcp_reply = reportObj.dcp_reply;
            this.dcp_reply_public = reportObj.dcp_reply_public;
            this.dcp_reply_time = DateTime.UtcNow;  //(reportObj.dcp_reply_time == null || reportObj.dcp_reply_time == DateTime.MinValue) ? DateTime.UtcNow : reportObj.dcp_reply_time;

            this.read_time = DateTime.UtcNow; // reportObj.read_time;
            this.sho_read_time = DateTime.UtcNow;// reportObj.sho_read_time;

            this.dcp_non_public_reply_time = DateTime.UtcNow;
            this.dcp_non_public_reply = "";

            this.imageid = reportObj.imageid;
            this.category = reportObj.category;
        }

        public ReportObjectEntity() { }

        public bool approval_stat { get; set; }
        public string policestation { get; set; }
        public DateTime date_time { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string location_name { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string details { get; set; }
        public string ipaddress { get; set; }

        public DateTime read_time { get; set; }
        //new properties
        public string sho_reply { get; set; }
        public DateTime sho_reply_time { get; set; }
        public string dcp_reply { get; set; }
        public bool dcp_reply_public { get; set; }
        public DateTime dcp_reply_time { get; set; }

        // Thomas - new fields
        public DateTime sho_read_time { get; set; }
        public bool sho_read_receipt { get; set; }

        public string dcp_non_public_reply { get; set; }
        public DateTime dcp_non_public_reply_time { get; set; }

        public DateTime? sms_send_time { get; set; }
        public bool sms_sent { get; set; }
        public string sms_sho_phone { get; set; }

        public string imageid { get; set; }
        public string category { get; set; }
    }

    public enum SubmissionStatus
    {
        SUCCESS, DUPLICATE, IPCHECK, LIMIT, FAILED
    }

    public class SmsObject
    {
        public ReportObjectEntity report { get; set; }
        public string phone { get; set; }
    }
    public class SimpleSearchResult
    {
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
