﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceData
{
    public class ServiceConfig
    {
        public static string BINGMAPS_KEY = "AhGbjtOz2XQfW3sFYXAira6uSVFO7dtIEam8DAWt7kfZNidQp2vMk0igWBk9Y1YH";
        public static string BINGMAPS_LOCALITY = "Delhi";
        public static string BINGMAPS_COUNTRY = "India";

        public static string SERVER_BASE_URL = "http://ogdimht.cloudapp.net/";
        public static string BINGMAPS_REQ_PREFIX = " " + BINGMAPS_LOCALITY.ToLower();
        public static string BINGMAPS_COUNTRY_REPLACE = ", "+BINGMAPS_LOCALITY+", "+BINGMAPS_COUNTRY;

        public static decimal[] BINGMAPS_GEO_FILTER_BOTTOMLEFT = new decimal[2] { (decimal)28.309217, (decimal)76.876831 };
        public static decimal[] BINGMAPS_GEO_FILTER_TOPRIGHT = new decimal[2] { (decimal)28.825425, (decimal)77.459106 };
        
        public static int FEEDBACK_PER_STATION_INTERVAL_MINUTES = 15; // Min. time betweek individual posts
        public static int FEEDBACK_PER_DAY_POST_LIMIT = 5;  // Max number of posts per day per IP

        public static string TABLE_POLICEDISTRICTS = "PoliceDistricts";
        public static string TABLE_POLICESTATIONS = "PoliceStations";
        public static string TABLE_CITIZENREPORTS = "CitizenReport";
        public static string TABLE_TWEETSREAD = "TweetsRead";

        public static string SMTP_SERVER_ADDR = "smtp.gmail.com";
        public static int SMTP_SERVER_PORT = 587;
        public static string EMAIL_SERVER_LOGIN = "delhipolice.kyps@gmail.com";
        public static string EMAIL_SERVER_PASSWORD = "kyps!@#$abcd";
        public static string IMAP_SERVER_ADDRESS = "imap.gmail.com";

        public static string TWITTER_HASHTAG = "DelhiPoliceKYPS";
        public static string TWITTER_CONSUMER_KEY = "7U58WP7hzx79UiTcakI68Q";
        public static string TWITTER_CONSUMER_SECRET = "t5UUdEH4IvPallvJfB0kLYvXmtZoyTd41CRW4gCss";
        public static string TWITTER_ACCESS_TOKEN = "488254440-QdsjplrbTzcgC6VUWZ7bUPM8O6y97mjW9DZuobXz";
        public static string TWITTER_ACCESS_SECRET = "yM7wfU3tK38EjKxxmV7A6hTKOs7TT1guxmUNLy3ASI";

        public static string TEMPL_SHO_EMAIL_SUBJECT = @"Feedback Number (%FEEDBACK_NUMBER%)";
        public static string TEMPL_SHO_EMAIL_BODY = @"Dear Sir,<br/>
This is to let you know that we have received a Citizen feedback for your Jurisdiction on (%FEEDBACK_DATE%).
<br/><br/> 
Citizen Name: %CITIZEN_NAME%<br/>
Phone: %CITIZEN_PHONE%<br/>
Email: %CITIZEN_EMAIL%<br/>
Location:%FEEDBACK_LOCATION%<br/>
Police Station: %FEEDBACK_PS%<br/>
Description of feedback: %FEEDBACK_DESC%<br/>
 <br/>
<b style='text-decoration:underline;'>Action required from you:</b><br/>
-          You are requested to take appropriate action and then kindly <b style='text-decoration:underline;'>reply</b> to this mail body with appropriate action mentioned in the mail. The system will accordingly record the same as having being noted by you.<br/>
<br/>
-          If the Citizen feedback is not actionable, then are required to reply back stating that “No action required” or any other appropriate wording. <b style='text-decoration:underline;'>Please do no change the subject line in your reply</b>.<br/>
-          Your reply/comments wil be sent for perusal of your district Addl. CP / DCP.<br/><br/>
<br/>
<br/>
This is an automated mail from the Know Your Police Station System.<br/>
%TRACKING_PIXEL%
<br/>
Thanking you.<br/>
";
        public static string TMPL_DCP_EMAIL_SUBJECT = @"Executive Summary: Feedback Summary for the Period ({0:dd-MM-yyyy}) to ({1:dd-MM-yyyy})";
        public static string TMPL_DCP_EMAIL_BODY = @"Dear Sir,<br/>
Please find below the summary of Citizen feedback by Police Station for your reference.<br/>
 <br/>

%PS_TABLES%
";
        public static string TMPL_DCP_EMAIL_TABLE = @"PS %PS_NAME%<br/>
<table>
<tr>
<th>Crime Head</th>
<th>Total for this Period</th>
<th>Total in this month</th>
<th>Total in Quarter</th>
<th>Total Received in current Year</th>
<th>Total actioned in current Year</th>
</tr>
%PS_ROWS%
</table>
";
        public static string TMPL_DCP_EMAIL_TABLE_ROW = @"<tr>
<td>%CRIME_HEAD%</td>
<td>%TOTAL%</td>
<td>%TOTAL_MONTH%</td>
<td>%TOTAL_QUARTER%</td>
<td>%TOTAL_YEAR%</td>
<td>%TOTAL_ACTIONED_YEAR%</td>
</tr>
";
    }
}