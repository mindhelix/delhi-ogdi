﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Google.Api.Maps.Service.Geocoding;
using Google.Api.Maps.Service;

namespace Ogdi.DataServices.Geocode
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGoogleMaps" in both code and config file together.
    [ServiceContract]
    public interface IGoogleMaps
    {
        [OperationContract]
        GeocodingResponse DoRequest(GeocodingRequest req);

        [OperationContract]
        GeocodingResponse ReverseGeocode(GeographicPosition pos);

        [OperationContract]
        string GetHello();

        [OperationContract]
        DrivingResponse GetDirections(String from, String to);
    }
}
