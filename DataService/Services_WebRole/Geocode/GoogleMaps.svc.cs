﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Google.Api.Maps.Service.Geocoding;
using Google.Api.Maps.Service;
using System.IO;
using Newtonsoft.Json;
using Ogdi.DataServices.BingMaps.Api;
using System.ServiceModel.Activation;

namespace Ogdi.DataServices.Geocode
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GoogleMaps : IGoogleMaps
    {
        public GeocodingResponse DoRequest(GeocodingRequest req)
        {
            //return null;
            req.Sensor = "false";
            var response = GeocodingService.GetResponse(req);
            var bing_response = BingGeocodingService.GetResponse(req);

            response.AppendResponse(bing_response);
            return response;
        }
        public DrivingResponse GetDirections(String from, String to)
        {
            var ApiUrl = "http://maps.googleapis.com/maps/api/directions/json?origin=" + from + "&destination=" + to
                + "&sensor=false";
            var url = new Uri(ApiUrl);
            var resp = Http.Get(url).As<DrivingResponse>();
            return resp;
        }
        public GeocodingResponse ReverseGeocode(GeographicPosition pos)
        {
            var req = new GeocodingRequest();
            req.LatitudeLongitude = pos.Latitude + "," + pos.Longitude;
            req.Sensor = "false";
            var response = GeocodingService.GetResponse(req);
            return response;
        }
        public string GetHello()
        {
            return "foo";
        }
    }
}
