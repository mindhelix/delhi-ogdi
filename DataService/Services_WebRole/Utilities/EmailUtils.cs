﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using Ogdi.DataServices.CitizenFeedback;
using Microsoft.WindowsAzure;
using Ogdi.Azure.Configuration;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Xml.Linq;
using Microsoft.WindowsAzure.StorageClient;
using ServiceData;

namespace Ogdi.DataServices.Utilities
{
    public class EmailUtils
    {
        public static void SendDCPEmail(ReportObjectEntity report)
        {
            
        }
        public static void SendSHOEmail(ReportObjectEntity report)
        {
            var psid = report.PartitionKey;

            var settings = StorageSettings.GetInstance();
            var client = settings.getCloudDataTableClient();
            var tableName = ServiceConfig.TABLE_POLICESTATIONS;
            var context = client.GetDataServiceContext();

            var station = (from g in context.CreateQuery<PoliceStation>(tableName)
                            where g.RowKey == psid
                            select g).Take(1).FirstOrDefault();
            string to = "";
            string to2 = "";
            
            if (station != null)
            {
                to = station.shoemail;
                tableName = ServiceConfig.TABLE_POLICEDISTRICTS;
                var district = (from d in context.CreateQuery<PoliceDistrictEntity>(tableName)
                                where d.id == station.districtid
                                select d).Take(1).FirstOrDefault();
                if (district != null)
                {
                    to2 = district.dcpemail;
                }
            
            }
            if (to == "" && to2 == "")
            {
                return;
            }

            var subject = ServiceConfig.TEMPL_SHO_EMAIL_SUBJECT;
            subject = subject.Replace("%FEEDBACK_NUMBER%", report.RowKey);

            var body = ServiceConfig.TEMPL_SHO_EMAIL_BODY;
            body = body.Replace("%FEEDBACK_DATE%", string.Format("{0:dd-MM-yyyy}",report.date_time));
            body = body.Replace("%CITIZEN_NAME%", report.name);
            body = body.Replace("%CITIZEN_PHONE%", report.phone);
            body = body.Replace("%CITIZEN_EMAIL%", report.email);
            body = body.Replace("%FEEDBACK_LOCATION%", report.location_name);
            body = body.Replace("%FEEDBACK_PS%", report.policestation);
            body = body.Replace("%FEEDBACK_DESC%",report.details);

            body = body.Replace("%TRACKING_PIXEL%", @"<img src='" + ServiceConfig.SERVER_BASE_URL + @"/ReadReceipt/'"+ report.RowKey + @"/>");

            SendEmail(to, subject, body, true);
            SendEmail(to2, subject, body, true);
        }
        public static void SendEmail(string to, string subject, string body, bool isHtml=false)
        {
            var storageAccount = CloudStorageAccount.Parse(OgdiConfiguration.GetValue("OgdiConfigConnectionString"));
            var queueStorage = storageAccount.CreateCloudQueueClient();
            var queue = queueStorage.GetQueueReference("workercommands");

            var messageXml = new XDocument(
                    new XElement("command",
                        new XAttribute("commandname", "SendMail"),
                        new XElement("EMailMessage",
                            new XElement("To", to),
                            //new XElement("To", "thomas@mindhelix.com"),
                            new XElement("Subject", subject),
                            new XElement("Body", body.ToString()),
                            new XElement("IsBodyHtml", "true")
                    )));

                var message = new CloudQueueMessage(messageXml.ToString());
                queue.AddMessage(message);

            return;

            MailMessage msg = new MailMessage();
            NetworkCredential cred = new NetworkCredential(ServiceConfig.EMAIL_SERVER_LOGIN, ServiceConfig.EMAIL_SERVER_PASSWORD);

            msg.From = new MailAddress(ServiceConfig.EMAIL_SERVER_LOGIN);

            msg.To.Add(to);
            //msg.To.Add("thomas@mindhelix.com");

            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = isHtml;

            SmtpClient smtpserver = new SmtpClient(ServiceConfig.SMTP_SERVER_ADDR);
            smtpserver.Port = ServiceConfig.SMTP_SERVER_PORT;
            smtpserver.Credentials = cred;
            smtpserver.EnableSsl = true;
            try
            {
                smtpserver.Send(msg);
                Console.WriteLine("\n Mail sent");
            }
            catch (SmtpException e)
            {
                Console.WriteLine(" {0} Exception Caught!!", e);
            }

        }
    }

    public class EmailMessage
    {
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}