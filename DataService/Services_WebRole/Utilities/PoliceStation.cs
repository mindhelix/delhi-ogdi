﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.StorageClient;

namespace Ogdi.DataServices.Utilities
{
    public class PoliceStation : TableServiceEntity
    {/*
        public PoliceStation(PSObject psObj)
        {
            this.PartitionKey = psObj.districtid.ToString();
            this.RowKey = psObj.id.ToString();
            this.entityid = Guid.NewGuid();

            this.id = psObj.id;
            this.stationname = psObj.stationname;
            this.districtid = psObj.districtid;
            this.shoid = psObj.shoid;
            this.shophone = psObj.shophone;
            this.shomobile = psObj.shomobile;
            this.shoemail = psObj.shoemail;
            this.latitude = psObj.latitude;
            this.longitude = psObj.longitude;
            this.kmlsnippet = psObj.kmlsnippet;
            this.address = psObj.address;
        }
        */
        public PoliceStation() { }

        public Guid entityid { get; set; }
        public int id { get; set; }
        public string stationname { get; set; }
        public int districtid { get; set; }
        public string shoid { get; set; }
        public string shophone { get; set; }
        public string shomobile { get; set; }
        public string shoemail { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string kmlsnippet { get; set; }
        public string address { get; set; }
    }
}