﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Twilio;
using ServiceData;

namespace Ogdi.DataServices.Utilities
{
    public class SmsUtils
    {
        //Twilio SID and auth token
        private static string accountSID = "AC25cfd2bf5ecdb56d1216a99b68ca43d8";
        private static string authToken = "d5c80fc4d1684b5549dfd45f0220a633";
        private static string twilioNumber = "+18477377992";

        // Send SMS from Twilio
        // ReportObjectEntity object is passed as parameter, which contains the details of the citizen feedback
        public static bool SendSMStoSHO(ReportObjectEntity report)
        {
            var sms_message = "New feedback received in category " + report.category + " from " + report.location_name;
            if (sms_message.Length >= 160)
            {
                sms_message = "New feedback received in category " + report.category;
            }

            var twilio = new TwilioRestClient(accountSID, authToken);
            var message = twilio.SendSmsMessage(twilioNumber, "+91" + report.sms_sho_phone, sms_message);
            
            // Check response. If null, no error.
            if (message != null)
            {
                if (message.RestException != null) //error details
                {
                    var error = message.RestException.Message;
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        }
    }
}