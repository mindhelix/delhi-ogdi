﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ogdi.DataServices
{
    public class ServiceConfig
    {

        public static string BINGMAPS_KEY = "AhGbjtOz2XQfW3sFYXAira6uSVFO7dtIEam8DAWt7kfZNidQp2vMk0igWBk9Y1YH";
        public static string BINGMAPS_LOCALITY = "Delhi";
        public static string BINGMAPS_COUNTRY = "India";

        public static string BINGMAPS_REQ_PREFIX = " " + BINGMAPS_LOCALITY.ToLower();
        public static string BINGMAPS_COUNTRY_REPLACE = ", " + BINGMAPS_LOCALITY + ", " + BINGMAPS_COUNTRY;

        // public static decimal[] BINGMAPS_GEO_FILTER_TOPLEFT = new decimal[2] { (decimal)28.830238, (decimal)76.860352 };
        // public static decimal[] BINGMAPS_GEO_FILTER_BOTTOMRIGHT = new decimal[2] { (decimal)28.32856, (decimal)77.459106 };

        public static int FEEDBACK_PER_STATION_INTERVAL_MINUTES = 15; // Min. time betweek individual posts
        public static int FEEDBACK_PER_DAY_POST_LIMIT = 5;  // Max number of posts per day per IP

        public static string TABLE_POLICEDISTRICTS = "PoliceDistricts";
        public static string TABLE_POLICESTATIONS = "PoliceStations";
        public static string TABLE_CITIZENREPORTS = "CitizenReport";

        public static string SMTP_SERVER_ADDR = "smtp.gmail.com";
        public static int SMTP_SERVER_PORT = 587;
        public static string SMTP_SERVER_LOGIN = "delhipolice.kyps@gmail.com";
        public static string SMTP_SERVER_PASSWORD = "kyps!@#$abcd";

        public static string TEMPL_SHO_EMAIL_SUBJECT = @"Feedback Number (%FEEDBACK_NUMBER%)";
        public static string TEMPL_SHO_EMAIL_BODY = @"Dear Sir,<br/>
This is to let you know that we have received a Citizen feedback for your Jurisdiction on (%FEEDBACK_DATE%).
<br/><br/> 
Citizen Name: %CITIZEN_NAME%<br/>
Phone: %CITIZEN_PHONE%<br/>
Email: %CITIZEN_EMAIL%<br/>
Location:%FEEDBACK_LOCATION%<br/>
Police Station: %FEEDBACK_PS%<br/>
Description of feedback: %FEEDBACK_DESC%<br/>
 <br/>
<b style='text-decoration:underline;'>Action required from you:</b><br/>
-          You are requested to take appropriate action and then kindly <b style='text-decoration:underline;'>reply</b> to this mail body with appropriate action mentioned in the mail. The system will accordingly record the same as having being noted by you.<br/>
<br/>
-          If the Citizen feedback is not actionable, then are required to reply back stating that “No action required” or any other appropriate wording. <b style='text-decoration:underline;'>Please do no change the subject line in your reply</b>.<br/>
-          Your reply/comments wil be sent for perusal of your district Addl. CP / DCP.<br/><br/>
<br/>
<br/>
This is an automated mail from the Know Your Police Station System.<br/>
<br/>
Thanking you.<br/>
";
    }
}