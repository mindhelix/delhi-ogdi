﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/*
 * Author : Thomas Antony
 * 
 * */

namespace Google.Api.Maps.Service.Geocoding
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Route
    {
        [JsonProperty("overview_polyline")]
        public Polyline point { get; set; }
    }
}