﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Newtonsoft.Json;
using ServiceData;
using System.Collections.Generic;

namespace Google.Api.Maps.Service.Geocoding
{
	[JsonObject(MemberSerialization.OptIn)]
	public class GeocodingResponse
	{
		[JsonProperty("status")]
		public ServiceResponseStatus Status { get; set; }

		[JsonProperty("results")]
		public GeocodingResult[] Results { get; set; }

        public GeocodingResponse AppendResponse(GeocodingResponse r)
        {
            if (r.Results!=null && r.Results.Length > 0)
            {
                var newArr = new GeocodingResult[Results.Length + r.Results.Length];
                Results.CopyTo(newArr, 0);
                r.Results.CopyTo(newArr, Results.Length);
                Results = newArr;
            }
            return this;
        }
        public SimpleSearchResult[] AsSimpleResult()
        {
            var list = new List<SimpleSearchResult>();
            int i = 0;
            // Filter values    
            var bl = ServiceConfig.BINGMAPS_GEO_FILTER_BOTTOMLEFT;// new decimal[2] { (decimal)28.309217, (decimal)76.876831 };// ,
            var tr = ServiceConfig.BINGMAPS_GEO_FILTER_TOPRIGHT; // new decimal[2] { (decimal)28.825425, (decimal)77.459106 };
            
            foreach(var result in this.Results)
            {
                if (result.Geometry.Location.Latitude > bl[0] && result.Geometry.Location.Longitude > bl[1]
                    && result.Geometry.Location.Latitude < tr[0] && result.Geometry.Location.Longitude < tr[1])
                {
                    if (result.FormattedAddress.Length > 50)
                    {
                        result.FormattedAddress = result.FormattedAddress.Substring(0, 50) + " ...";
                    }
                    var obj = new SimpleSearchResult
                    {
                        Address = result.FormattedAddress,
                        Latitude = result.Geometry.Location.Latitude,
                        Longitude = result.Geometry.Location.Longitude
                    };
                    list.Add(obj);
                }
            }
            return list.ToArray();
        }
	}
}
