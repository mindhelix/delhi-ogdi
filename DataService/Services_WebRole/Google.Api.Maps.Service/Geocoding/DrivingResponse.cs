﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Google.Api.Maps.Service;

/*
 * Author : Thomas Antony
 * 
 * */
namespace Google.Api.Maps.Service.Geocoding
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DrivingResponse
    {
        [JsonProperty("status")]
        public ServiceResponseStatus Status { get; set; }

        [JsonProperty("routes")]
        public Route[] Routes { get; set; }
    }
}