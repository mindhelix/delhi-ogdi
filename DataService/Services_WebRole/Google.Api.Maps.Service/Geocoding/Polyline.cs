﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Google.Api.Maps.Service;
using System.Collections.ObjectModel;

/*
 * Author : Thomas Antony
 * 
 * */

namespace Google.Api.Maps.Service.Geocoding
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Polyline
    {
        [JsonProperty("points")]
        public string encoded_str {get; set;}

        public GeographicPosition[] points
        {
            get
            {
                var list = new List<GeographicPosition>();
                DecodeGPolylineToLocationCollection(encoded_str, list);
                return list.ToArray();
            }
        }
        private void DecodeGPolylineToLocationCollection(string gPolyString, List<GeographicPosition> GPolyLocationCollection)
        {
            //
            // The caller may have sent us an empty string
            //

            if (String.IsNullOrWhiteSpace(gPolyString))
                return;

            var GPolyElementList = DecodeGPolyline(gPolyString);

            if ((GPolyElementList.Count % 2) != 0)
                throw new ArgumentException();

            if (GPolyElementList.Count == 0)
                return;

            var LastLocation = GeographicPosition.FromLatLon(0.0, 0.0);
            bool IsLat = true;
            GeographicPosition ElLoc = null;

            foreach (var El in GPolyElementList)
            {
                if (IsLat)
                {
                    ElLoc = new GeographicPosition();

                    ElLoc.Latitude = (decimal) (El + (double) LastLocation.Latitude);
                    IsLat = false;
                }
                else
                {
                    ElLoc.Longitude = (decimal)(El + (double)LastLocation.Longitude);
                    IsLat = true;
                    GPolyLocationCollection.Add(ElLoc);
                    LastLocation = ElLoc;
                }
            }

            return;
        }
        private Collection<double> DecodeGPolyline(string gPolyString)
        {
            const int MaxGPolyChunks = 7;
            var GPolyElementList = new Collection<double>();

            //
            // Step 11: Convert the GPolyline string back into an array of chunks
            //

            var GPolyBytes = Encoding.UTF8.GetBytes(gPolyString);

            //
            // Walk through the array of chunks splitting it into a list of elements
            //

            var TempChunk = new byte[MaxGPolyChunks];
            int TempChunkIndex = 0;

            for (int i = 0; i < GPolyBytes.Length; i++)
            {
                //
                // A element cannot have more than 7 chunks
                //

                if (TempChunkIndex >= MaxGPolyChunks)
                    throw new ArgumentOutOfRangeException();

                //
                // Step 10 & 9: Chunks are ASCII "base" 63 values
                //

                if (GPolyBytes[i] < 63)
                    throw new ArgumentOutOfRangeException();

                TempChunk[TempChunkIndex] = (byte)(GPolyBytes[i] - 63);

                //
                // A chunk is a 6 bit value: 5 bits data with an upper continuation bit
                //

                if ((TempChunk[TempChunkIndex] & 0xC0) != 0)
                    throw new ArgumentOutOfRangeException();

                if ((TempChunk[TempChunkIndex] & 0x20) != 0)
                {
                    //
                    // Step 8: Clear the continuation bit
                    //

                    TempChunk[TempChunkIndex] &= 0x1F;

                    TempChunkIndex++;

                    continue;
                }

                //
                // Step 7 & 6: Reverse the chunk order and convert to a 32-bit integer
                //

                uint PolyPointDecimal = 0;

                for (var l = 0; l < TempChunkIndex + 1; l++)
                {
                    PolyPointDecimal += (uint)((1 << (5 * l)) * TempChunk[l]);
                }

                //
                // Step 5: If the low order bit is set, the original value was negative, invert.
                //

                if ((PolyPointDecimal & 0x1) == 1)
                    PolyPointDecimal = ~PolyPointDecimal;

                //
                // Step 4: Right shift the value 1 bit
                // Step 3 & 2: Convert back to floating point by dividing by 1e5
                //

                var PolyPoint = (double)((int)PolyPointDecimal >> 1) / 100000;

                //
                // Add chunks to the element list
                //

                GPolyElementList.Add(PolyPoint);

                //
                // Start a new chunk
                //

                Array.Clear(TempChunk, 0, MaxGPolyChunks);
                TempChunkIndex = 0;
            }

            //
            // The last chunk in the GPolyline must not be a continution chunk
            //

            if (TempChunkIndex != 0)
                throw new ArgumentOutOfRangeException();

            return GPolyElementList;
        }
    }
}
