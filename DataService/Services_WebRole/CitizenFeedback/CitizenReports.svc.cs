﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;
using Ogdi.DataServices.Utilities;
using System.ServiceModel.Activation;
using ServiceData;

namespace Ogdi.DataServices.CitizenFeedback
{
    //-------- CitizenReports Services class
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CitizenReports : ICitizenReports
    {
        public static int PER_STATION_INTERVAL_MINUTES = 15; // Min. time betweek individual posts
        public static int PER_DAY_POST_LIMIT = 10;

        //Azure Table Storage settings initialized
        StorageSettings storageSettings;
        CloudTableClient tableClient;
        string tableName;

        public static int STATUS_OK = 0;
        public static int STATUS_DUPLICATE = 1;
        public static int STATUS_IPCHECK = 2;

        public CitizenReports()
        {
            storageSettings = StorageSettings.GetInstance();
            tableClient = storageSettings.getCloudTableClient();
            tableName = "CitizenReport";
        }


        //Post Citizen Reports
        public SubmissionStatus postCitizenReport(ReportObject reportObj)
        {
            ReportObjectEntity repObjEntity = new ReportObjectEntity(reportObj);
            tableClient.CreateTableIfNotExist(tableName);

            
            try
            {
                TableServiceContext tableContext = tableClient.GetDataServiceContext();

                var reportObjects = (from e in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                     where e.PartitionKey == reportObj.ps_id.ToString()
                                     select e).AsEnumerable().ToArray();

                var beforeinterval = DateTime.UtcNow.AddMinutes(-PER_STATION_INTERVAL_MINUTES); // One report per 15 minutes per station per ip
                var newobj = reportObjects.Where(a => a.date_time.CompareTo(beforeinterval) > 0 && a.ipaddress == repObjEntity.ipaddress);
                if (newobj.Count() > 0)
                {
                    return SubmissionStatus.IPCHECK;// One report per hour per station per ip allowed
                }

                var onedayago = DateTime.UtcNow.AddDays(-1);
                var onedayreports = (from e in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                     where e.RowKey.CompareTo(onedayago.Ticks.ToString()) > 0
                                     select e).AsEnumerable().ToArray();
                var limitcheck = onedayreports.Where(a => a.ipaddress!=null && a.ipaddress.Equals(repObjEntity.ipaddress));

                if (limitcheck.Count() >= PER_DAY_POST_LIMIT)
                {
                    return SubmissionStatus.LIMIT;  // Per day limit exceeded.
                }
                var dupes = reportObjects.Where(a => a.email == reportObj.email && a.details == reportObj.details);
                if (dupes.Count() > 0)
                {
                    return SubmissionStatus.DUPLICATE;
                }
                tableContext.AddObject(tableName, repObjEntity);
                tableContext.SaveChangesWithRetries();

                EmailUtils.SendSHOEmail(repObjEntity);

                // New SMS Utility added by Jinsu 04-Nov-2013
                SmsUtils.SendSMStoSHO(repObjEntity);

                return SubmissionStatus.SUCCESS;
            }
            catch (Exception e)
            {
                return SubmissionStatus.FAILED;
            }
        }


        //Get All Citizen Reports
        public IEnumerable<ReportObject> getCitizenReports()
        {
            TableServiceContext tableContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportObject> reportObjects = (from e in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                                       select new ReportObject
                                                       {
                                                           ps_id = Int32.Parse(e.PartitionKey),
                                                           date_time = e.date_time,
                                                           latitude = e.latitude,
                                                           longitude = e.longitude,
                                                           location_name = e.location_name,
                                                           phone = e.phone,
                                                           email = e.email,
                                                           name = e.name,
                                                           details = e.details
                                                       }).AsEnumerable<ReportObject>();

            return reportObjects;
        }


        //Get Citizen Reports
        public IEnumerable<ReportObject> getCitizenReportByPoliceStation(int policeStationId)
        {
            TableServiceContext tableContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportObject> reportObjects = (from e in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                                       where e.PartitionKey == policeStationId.ToString()
                                                       select new ReportObject
                                                       {
                                                           ps_id = Int32.Parse(e.PartitionKey),
                                                           date_time = e.date_time,
                                                           latitude = e.latitude,
                                                           longitude = e.longitude,
                                                           location_name = e.location_name,
                                                           phone = e.phone,
                                                           email = e.email,
                                                           name = e.name,
                                                           details = e.details
                                                       }).AsEnumerable<ReportObject>();

            return reportObjects;
        }
        public int getCitizenReportCountByPoliceStation(int policeStationId)
        {
            TableServiceContext tableContext = tableClient.GetDataServiceContext();

            try
            {
                var res = (from e in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                           where e.PartitionKey == policeStationId.ToString()
                           select e);
                int count = res.ToArray().Count();
                return count;
            }
            catch (Exception e) { return 0; }
        }

        public List<ReportObjectEntity> GetFeedbacksForSms(DateTime start)
        {
            var repContext = tableClient.GetDataServiceContext();
            start = start.ToUniversalTime();
            var res = (from r in repContext.CreateQuery<ReportObjectEntity>(tableName)
                       where r.date_time.CompareTo(start) > 0
                       select r);
            res = res.Where(r => r.sms_sent == false);
            
            return res.ToList();
        }
        public void SetFeedbackSMSSent(string num)
        {
            var repContext = tableClient.GetDataServiceContext();
            var res = (from r in repContext.CreateQuery<ReportObjectEntity>(tableName)
                       where r.RowKey == num
                       select r).FirstOrDefault();
            if (res != null)
            {
                res.sms_sent = true;
                res.sms_send_time = DateTime.UtcNow;
                repContext.UpdateObject(res);
                repContext.SaveChanges();
            }
        }
    }
}
