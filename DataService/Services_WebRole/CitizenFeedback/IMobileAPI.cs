﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;
using Ogdi.DataServices.Utilities;
using ServiceData;
using Google.Api.Maps.Service.Geocoding;
using ServiceData.Utilities;

namespace Ogdi.DataServices.CitizenFeedback
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMobileAPI" in both code and config file together.
    [ServiceContract]
    public interface IMobileAPI
    {
        [WebGet(UriTemplate = "/dowork",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string DoWork();

        [WebInvoke(UriTemplate = "/getpsforlocation",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        SimplePSEntity GetPsForLocation(Location loc);

        [WebInvoke(UriTemplate = "/uploadphoto",
            ResponseFormat = WebMessageFormat.Json)]
        string UploadPhoto(Stream imagedata);

        [WebInvoke(UriTemplate = "/submitfeedback",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        SubmissionStatus SubmitFeedback(MobileReportObject obj);

        [WebGet(UriTemplate = "/getsmsfeedback",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        List<SmsObject> GetFeedbacksForSms();

        [WebInvoke(UriTemplate = "/search",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        SimpleSearchResult[] DoSearchRequest(GeocodingRequest req);
    }
    [DataContract]
    public class MobileReportObject
    {
        [DataMember]
        public int ps_id { get; set; }

        [DataMember]
        public string details { get; set; }

        [DataMember]
        public double latitude { get; set; }

        [DataMember]
        public double longitude { get; set; }

        [DataMember]
        public string location_name { get; set; }

        [DataMember]
        public string policestation { get; set; }

        [DataMember]
        public string imageid { get; set; }

        [DataMember]
        public string category { get; set; }
    }
}
