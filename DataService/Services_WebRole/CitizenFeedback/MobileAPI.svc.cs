﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.IO;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Ogdi.DataServices.Utilities;
using System.Web;
using System.Xml.Linq;
using System.ServiceModel.Channels;
using ServiceData;
using ServiceData.Utilities;
using Google.Api.Maps.Service.Geocoding;
using Ogdi.DataServices.BingMaps.Api;
using System.ServiceModel.Web;

namespace Ogdi.DataServices.CitizenFeedback
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MobileAPI" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MobileAPI : IMobileAPI
    {
        protected const string connectionStringName = "OgdiConfigConnectionString";
        private const string voiceNoteBlobName = "feedbackphotos";
        private CloudBlobClient blobClient;
        private CloudBlobContainer blobContainer;
        public CloudStorageAccount storageAccount;

        public MobileAPI()
        {
            // Check if cache contains station data, if not, refresh it.
            if (HttpRuntime.Cache["POLICE_STATIONS"] == null || HttpRuntime.Cache["STATION_POLYGONS"] == null)
            {
                var settings = StorageSettings.GetInstance();
                var client = settings.getCloudDataTableClient();
                var tableName = ServiceConfig.TABLE_POLICESTATIONS;
                var context = client.GetDataServiceContext();

                var stations = (from g in context.CreateQuery<PoliceStation>(tableName)
                                select g).ToList<PoliceStation>();
                HttpRuntime.Cache["POLICE_STATIONS"] = stations;
                HttpRuntime.Cache["STATION_POLYGONS"] = PolygonUtils.ParseStations(stations);
            }
        }
        public SimplePSEntity GetPsForLocation(Location loc)
        {
            var polygons = (List<Polygon>)HttpRuntime.Cache["STATION_POLYGONS"];
            return PolygonUtils.FindStationContainingLocation(polygons, loc);
        }
        public string DoWork()
        {
            var o = new MobileReportObject();
            o.details = "Testing sms feedback";
            o.imageid = "";
            o.location_name = "Kotwali";
            o.policestation = "Kotwali";
            o.ps_id = 101;
            o.latitude = 28.655921;
            o.longitude = 77.23704;
            o.category = "Other Unlawful activity";
            SubmitFeedback(o);
            return "yes";

        }
        public SubmissionStatus SubmitFeedback(MobileReportObject obj)
        {
            var reports = new CitizenReports();
            var rep = new ReportObject
            {
                ps_id = obj.ps_id,
                policestation = obj.policestation,
                details = obj.details,
                latitude = obj.latitude,
                longitude = obj.longitude,
                location_name = obj.location_name,
                imageid = obj.imageid,
                category = obj.category
            };
            rep.name = "";
            rep.phone = "";
            rep.email = "";

            rep.date_time = DateTime.UtcNow;

            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            rep.ipaddress = endpoint.Address;

            return reports.postCitizenReport(rep);
        }
        public string UploadPhoto(Stream imagedata)
        {
            string connectionString = RoleEnvironment.GetConfigurationSettingValue(connectionStringName);
            storageAccount = CloudStorageAccount.Parse(connectionString);

            blobClient = storageAccount.CreateCloudBlobClient();
            blobContainer = blobClient.GetContainerReference(voiceNoteBlobName);
            blobContainer.CreateIfNotExist();
            string uniqueBlobName = string.Format(voiceNoteBlobName + "/photo-{0}{1}", Guid.NewGuid().ToString(), ".jpg");
            CloudBlockBlob blob = blobClient.GetBlockBlobReference(uniqueBlobName);
            blob.Properties.ContentType = "image/jpeg";
            blob.UploadFromStream(imagedata);

            return uniqueBlobName;
        }
        public List<SmsObject> GetFeedbacksForSms()
        {
            var reports = new CitizenReports();
            reports.GetFeedbacksForSms(DateTime.UtcNow.AddDays(-1));
            return null;
        }
        public SimpleSearchResult[] DoSearchRequest(GeocodingRequest req)
        {
            //return null;
            req.Sensor = "false";
            var response = GeocodingService.GetResponse(req);
            var bing_response = BingGeocodingService.GetResponse(req);

            response.AppendResponse(bing_response);
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");

            return response.AsSimpleResult();
        }
    }
}
