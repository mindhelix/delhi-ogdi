﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.WindowsAzure.StorageClient;
using ServiceData;

namespace Ogdi.DataServices.CitizenFeedback
{
    public partial class ReadReceipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string feedbackid = (string)Page.RouteData.Values["feedbackid"];

            if (feedbackid != null)
            {
                var storage = StorageSettings.GetInstance();
                var tableClient = storage.getCloudTableClient();
                var tableName = "CitizenReport";
                TableServiceContext tableContext = tableClient.GetDataServiceContext();

                var reportObject = (from r in tableContext.CreateQuery<ReportObjectEntity>(tableName)
                                    where r.RowKey == feedbackid
                                    select r).FirstOrDefault();
                if (reportObject != null)
                {
                    reportObject.sho_read_receipt = true;
                    reportObject.sho_read_time = DateTime.UtcNow;
                    tableContext.UpdateObject(reportObject);
                    tableContext.SaveChanges();
                }
                Response.ContentType = "image/jpeg";
            }
            else
            {
                Response.Write("Fail");
            }
        }
    }
}