﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;
using System.Web;
using System.ServiceModel.Channels;
using ServiceData;

namespace Ogdi.DataServices.CitizenFeedback
{
    //------- Citizen Reports service interface
    
    [ServiceContract]
    public interface ICitizenReports
    {
        [OperationContract]
        SubmissionStatus postCitizenReport(ReportObject reportObj);

        [OperationContract]
        IEnumerable<ReportObject> getCitizenReports();

        [OperationContract]
        IEnumerable<ReportObject> getCitizenReportByPoliceStation(int policeStationId);

        [OperationContract]
        int getCitizenReportCountByPoliceStation(int policeStationId);

        [OperationContract]
        List<ReportObjectEntity> GetFeedbacksForSms(DateTime start);

        [OperationContract]
        void SetFeedbackSMSSent(string num);
    }



}
