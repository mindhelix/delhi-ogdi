﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;
using SnowMaker;

namespace Ogdi.DataServices.CitizenFeedback
{
    public class StorageSettings
    {
        private static StorageSettings instance = null;
        public static StorageSettings GetInstance()
        {
            if (instance == null)
            {
                instance = new StorageSettings();
            }
            return instance;
        }
        public UniqueIdGenerator GetIdGenerator()
        {
            return generator;
        }
        private StorageSettings()
        {
            connectionString = "DataConnectionString";
            //storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=dpogdiconfigtest;AccountKey=J9hg9YLGToozjhogtulszWWvOVj1PXBZfWCvNR068lZE3Qgc2BIbCzs9z8ju0xSFgqg5PlU4fWki7unOITWkoQ==");
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionString));
            tableClient = storageAccount.CreateCloudTableClient();

            var cs = RoleEnvironment.GetConfigurationSettingValue("OgdiConnectionString");
            dataStorageAccount = CloudStorageAccount.Parse(cs);
            dataTableClient = dataStorageAccount.CreateCloudTableClient();

            generator = new UniqueIdGenerator(storageAccount);
            generator.BatchSize = 1;
        }

        UniqueIdGenerator generator = null;
        string connectionString;
        CloudStorageAccount storageAccount, dataStorageAccount;
        CloudTableClient tableClient, dataTableClient;

        public CloudTableClient getCloudTableClient() { return tableClient; }
        public CloudTableClient getCloudDataTableClient() { return dataTableClient; }
    }
}