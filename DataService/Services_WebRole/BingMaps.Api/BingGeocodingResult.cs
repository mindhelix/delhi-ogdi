﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Api.Maps.Service.Geocoding;
using Newtonsoft.Json;

namespace Ogdi.DataServices.BingMaps.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BingGeocodingResult
    {
        [JsonProperty("address")]
        public BingGeocodingAddress address { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("point")]
        public BingGeocodingPoint point { get; set; }
    }
}