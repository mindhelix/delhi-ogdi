﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Google.Api.Maps.Service;

namespace Ogdi.DataServices.BingMaps.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BingGeocodingPoint
    {
        [JsonProperty("coordinates")]
        public decimal[] coordinates { get; set; }
        public GeographicPosition AsGeographicPosition()
        {
            var obj = new GeographicPosition();
            obj.Latitude = coordinates[0];
            obj.Longitude = coordinates[1];
            return obj;
        }
    }
}