﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Ogdi.DataServices.BingMaps.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BingGeocodingAddress
    {
        [JsonProperty("adminDistrict")]
        public string addressLine { get; set; }
        [JsonProperty("countryRegion")]
        public string countryRegion { get; set; }
        [JsonProperty("formattedAddress")]
        public string formattedAddress { get; set; }
        [JsonProperty("locality")]
        public string locality { get; set; }
        [JsonProperty("postalCode")]
        public string postalCode { get; set; }
    }
}