﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Api.Maps.Service;
using Google.Api.Maps.Service.Geocoding;
using ServiceData;

namespace Ogdi.DataServices.BingMaps.Api
{
    public class BingGeocodingService
    {
        public static GeocodingResponse GetResponse(GeocodingRequest req)
        {
            Uri uri = new Uri("http://dev.virtualearth.net/REST/v1/Locations?query=" + req.Address +ServiceConfig.BINGMAPS_REQ_PREFIX
                            + "&includeNeighborhood=0&key="+ServiceConfig.BINGMAPS_KEY);
            return Http.Get(uri).As<BingGeocodingResponse>().AsGeocodingResponse();
        }
    }
}