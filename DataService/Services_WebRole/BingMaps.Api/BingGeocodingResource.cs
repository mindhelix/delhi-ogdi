﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Ogdi.DataServices.BingMaps.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BingGeocodingResource
    {
        [JsonProperty("estimatedTotal")]
        public int estimatedTotal { get;set; }

        [JsonProperty("resources")]
        public BingGeocodingResult[] Results { get; set; }
    }
}