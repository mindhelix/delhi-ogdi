﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Google.Api.Maps.Service.Geocoding;
using Google.Api.Maps.Service;
using ServiceData;

namespace Ogdi.DataServices.BingMaps.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BingGeocodingResponse
    {
        [JsonProperty("statusDescription")]
        public string Status { get; set; }

        [JsonProperty("resourceSets")]
        public BingGeocodingResource[] ResourceSets { get; set; }

        public GeocodingResponse AsGeocodingResponse()
        {
            var obj = new GeocodingResponse();
            obj.Status = ServiceResponseStatus.Ok;
            if (ResourceSets != null && ResourceSets.Count() > 0)
            {            
                obj.Results = new GeocodingResult[ResourceSets[0].Results.Count()];
                var i = 0;
                foreach (var res in ResourceSets[0].Results)
                {
                    obj.Results[i] = new GeocodingResult();
                    
                    if (res.address != null)
                    {
                        
                        if(!res.name.Contains(ServiceConfig.BINGMAPS_LOCALITY)){
                            obj.Results[i].FormattedAddress = res.name.Replace(", "+ServiceConfig.BINGMAPS_COUNTRY, ServiceConfig.BINGMAPS_COUNTRY_REPLACE);
                        }else{
                            obj.Results[i].FormattedAddress = res.name;
                        }
                    }
                    obj.Results[i].Geometry = new Geometry();
                    if(res.point != null)
                        obj.Results[i].Geometry.Location = res.point.AsGeographicPosition();
                    
                    i++;
                }
            }
            return obj;
        }
        
    }
}